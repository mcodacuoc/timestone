package uoc.edu.timestone.model;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

/**
 * This class represents the bluetooth device information
 */
public class Device implements Serializable {
    public enum DeviceType {
        DEVICE_TYPE_ENTRANCE,
        DEVICE_TYPE_INTERIOR
    }

    private String id;
    private int major;
    private int minor;
    private String centerId;
    private DeviceType type;
    @Exclude
    private double distance;

    /**
     * Device class empty constructor
     */
    public Device() {
    }

    /**
     * Device class parameter constructor
     *
     * @param uuid   The bluetooth device identifier
     * @param major  The device major number
     * @param minor  The device mino number
     * @param type   The type of location for this device
     * @param center The center in wich the device is located
     */
    public Device(String uuid, int major, int minor, DeviceType type, Center center) {
        this.id = uuid;
        this.major = major;
        this.minor = minor;
        this.centerId = center.getId();
        this.type = type;
    }

    public Device(String uuid, int major, int minor, DeviceType type) {
        this.id = uuid;
        this.major = major;
        this.minor = minor;
        this.type = type;
        this.centerId = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public DeviceType getType() {
        return type;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
