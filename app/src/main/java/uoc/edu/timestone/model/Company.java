package uoc.edu.timestone.model;

/**
 * This class represents a basic company information
 */
public class Company {
    private String id;
    private String name;

    /**
     * The Company class empty constructor
     */
    public Company() {
    }

    /**
     * The Company class parameter constructor
     *
     * @param cif  The legal company identifier
     * @param name The name of the company
     */
    public Company(String cif, String name) {
        this.id = cif;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
