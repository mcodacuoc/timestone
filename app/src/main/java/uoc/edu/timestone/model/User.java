package uoc.edu.timestone.model;

import java.io.Serializable;

/**
 * This class represents an user registered in system
 */
public class User implements Serializable {
    private String id;
    private String email;
    private String password;
    private String name;
    private String surname1;
    private String surname2;
    private String position;
    private int dayhours;

    /**
     * User class empty constructor
     */
    public User() {
    }

    /**
     * User class parameter constructor
     *
     * @param id       The user DNI identification
     * @param email    The user email.
     * @param password The user password
     * @param name     The user name
     * @param surname1 The user first surname
     * @param surname2 The user second surname
     * @param position The user position in company
     * @param dayhours The day working hours stipulated by contract
     */
    public User(String id, String email, String password, String name, String surname1,
                String surname2, String position, int dayhours) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname1 = surname1;
        this.surname2 = surname2;
        this.position = position;
        this.dayhours = dayhours;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname1() {
        return surname1;
    }

    public String getSurname2() {
        return surname2;
    }

    public String getPosition() {
        return position;
    }

    public int getDayhours() {
        return dayhours;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname1(String surname1) {
        this.surname1 = surname1;
    }

    public void setSurname2(String surname2) {
        this.surname2 = surname2;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setDayhours(int dayhours) {
        this.dayhours = dayhours;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) {
            return false;
        } else {
            User other = (User) o;

            if (!this.id.equals(other.id)) {
                return false;
            } else {
                return true;
            }
        }
    }

    @Override
    public String toString() {
        return name + " " + surname1 + " " + surname2;
    }
}
