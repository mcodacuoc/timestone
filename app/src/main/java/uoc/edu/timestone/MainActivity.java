package uoc.edu.timestone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import uoc.edu.timestone.ui.device.DeviceConfigActivity;
import uoc.edu.timestone.ui.system.SystemConfigActivity;
import uoc.edu.timestone.ui.user.UserInfoActivity;
import uoc.edu.timestone.ui.user.UsersActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        }, intentFilter);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_notifications, R.id.navigation_workday,
                R.id.navigation_analysis, R.id.navigation_report)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        if (!MainEmptyActivity.mCurrentUser.getPosition().equals("admin")) {
            MenuItem item = menu.findItem(R.id.device_config);
            item.setVisible(false);
        }

        if (!MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
            MenuItem item = menu.findItem(R.id.system_config);
            item.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent activityIntent;

        switch (item.getItemId()) {
            case R.id.user_config:
                if (!MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
                    activityIntent = new Intent(this, UserInfoActivity.class);
                    activityIntent.putExtra(UserInfoActivity.SELECTED_USER,
                            MainEmptyActivity.mCurrentUser);
                    startActivity(activityIntent);
                } else {
                    activityIntent = new Intent(this, UsersActivity.class);
                    startActivity(activityIntent);
                }
                break;
            case R.id.system_config:
                activityIntent = new Intent(this, SystemConfigActivity.class);
                startActivity(activityIntent);
                break;
            case R.id.device_config:
                activityIntent = new Intent(this, DeviceConfigActivity.class);
                startActivity(activityIntent);
                break;
        }

        return true;
    }
}
