package uoc.edu.timestone.ui.device;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Device;

public class DeviceEditActivity extends AppCompatActivity {
    public static final String EDIT_MODE = "edit";
    public static final String SELECTED_DEVICE = "device";
    public static final String REMOVE_ACTION = "remove_flag";
    public static final int NEW_DEVICE_REQUEST = 1;
    public static final int EDIT_DEVICE_REQUEST = 2;

    private boolean isEdit = false;
    private Device mDevice = null;

    private TextView mUUIDValueLine;
    private TextView mMajorValueLine;
    private TextView mMinorValueLine;
    private Spinner mCenterSelect;
    private RadioButton mTypeEntrance;
    private RadioButton mTypeInterior;

    private CenterManager mCenterManager;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_edit);

        mContext = this;

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load activity control references
        mUUIDValueLine = findViewById(R.id.uuidValueTextLine);
        mMajorValueLine = findViewById(R.id.majorValueTextLine);
        mMinorValueLine = findViewById(R.id.minorValueTextLine);
        mCenterSelect = findViewById(R.id.centerSelectSpinner);
        mTypeEntrance = findViewById(R.id.entranceType);
        mTypeInterior = findViewById(R.id.interiorType);

        // Center database value change will reload center select spinner
        mCenterManager = new CenterManager(FirebaseDatabase.getInstance());
        mCenterManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCenterSelect.setAdapter(new ArrayAdapter<>(
                        mContext, android.R.layout.simple_spinner_dropdown_item,
                        mCenterManager.getCenters()));

                if (null != mDevice) {
                    for (int i = 0; i < mCenterManager.getCenters().size(); i++) {
                        if (mCenterManager.getCenters().get(i).getId().equals(mDevice.getCenterId())) {
                            mCenterSelect.setSelection(i);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Get edition intent parameter
        isEdit = getIntent().getBooleanExtra(EDIT_MODE, false);

        // If event have been opened in edition mode, then event information is loaded.
        if (isEdit) {
            // The activity title shall be edit
            setTitle(R.string.title_activity_edit);
            mDevice = (Device) getIntent().getSerializableExtra(SELECTED_DEVICE);
            mUUIDValueLine.setText(mDevice.getId());
            mMajorValueLine.setText(String.valueOf(mDevice.getMajor()));
            mMinorValueLine.setText(String.valueOf(mDevice.getMinor()));

            if (mDevice.getType().equals(Device.DeviceType.DEVICE_TYPE_ENTRANCE)) {
                mTypeEntrance.setChecked(true);
            } else {
                mTypeInterior.setChecked(true);
            }
        } else {
            // In creation mode, title shall be new
            setTitle(R.string.title_activity_new);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        if (!isEdit) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent returnIntent = new Intent();
        switch (item.getItemId()) {
            // If save action have been performed, the activity will finish returning
            // the edited or created device
            case R.id.action_save:
                if (saveDevice()) {
                    returnIntent.putExtra(REMOVE_ACTION, false);
                    returnIntent.putExtra(SELECTED_DEVICE, mDevice);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Snackbar.make(this.findViewById(android.R.id.content),
                            getString(R.string.empty_values_error),
                            Snackbar.LENGTH_SHORT).show();
                }
                return true;

            //  If action performed was delete, then selected device will be returned
            case R.id.action_delete:
                returnIntent.putExtra(REMOVE_ACTION, true);
                returnIntent.putExtra(SELECTED_DEVICE, mDevice);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;

            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method saves introduced data in a new or edited device.
     *
     * @return <code>true</code> if information is valid.
     */
    private boolean saveDevice() {
        boolean result = false;
        if (isValid()) {
            if (!isEdit) {
                mDevice = new Device();
            }
            mDevice.setId(mUUIDValueLine.getText().toString());
            mDevice.setMajor(Integer.parseInt(mMajorValueLine.getText().toString()));
            mDevice.setMinor(Integer.parseInt(mMinorValueLine.getText().toString()));

            Center center = mCenterManager.getCenters().get(mCenterSelect.getSelectedItemPosition());
            mDevice.setCenterId(center.getId());

            if (mTypeEntrance.isChecked()) {
                mDevice.setType(Device.DeviceType.DEVICE_TYPE_ENTRANCE);
            } else {
                mDevice.setType(Device.DeviceType.DEVICE_TYPE_INTERIOR);
            }
            result = true;
        }
        return result;
    }

    /**
     * This method tells whether introduced information os valid
     *
     * @return <code>true</code> if information is valid
     */
    private boolean isValid() {
        boolean result = true;
        if (0 == mUUIDValueLine.length() || 0 == mMajorValueLine.length() ||
                0 == mMinorValueLine.length()) {
            result = false;
        }
        return result;
    }
}
