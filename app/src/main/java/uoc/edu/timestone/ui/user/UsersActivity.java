package uoc.edu.timestone.ui.user;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.UserListAdapter;
import uoc.edu.timestone.model.User;

import android.view.MenuItem;
import android.view.View;

/**
 * This class implements user list activity window
 */
public class UsersActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private UserListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load recycler view
        recyclerView = findViewById(R.id.user_list_recycler_view);

        // Specify an adapter (see also next example)
        adapter = new UserListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // On floating action click, an user edit activity will be started.
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activityIntent = new Intent(view.getContext(), UserEditActivity.class);
                activityIntent.putExtra(UserEditActivity.EDIT_MODE, false);
                startActivityForResult(activityIntent, UserEditActivity.NEW_CONTACT_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Make sure the request was successful
        if (resultCode == RESULT_OK) {
            if (requestCode == UserEditActivity.NEW_CONTACT_REQUEST) {
                // For new contact request, calls user manager add method
                User user = (User) data.getSerializableExtra(UserEditActivity.SELECTED_USER);
                adapter.addUser(user);

            } else if (requestCode == UserEditActivity.EDIT_CONTACT_REQUEST) {
                // For remove or update contact request, calls user manager appropriate method
                User user = (User) data.getSerializableExtra(UserEditActivity.SELECTED_USER);
                boolean remove = data.getBooleanExtra(UserEditActivity.REMOVE_ACTION, false);

                if (remove) {
                    adapter.removeUser(user);
                } else {
                    adapter.updateUser(user);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
