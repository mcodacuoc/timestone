package uoc.edu.timestone.ui.report;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.ReportListAdapter;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.utils.ReportUtils;
import uoc.edu.timestone.utils.WorkingHourUtils;

/**
 * This class implements the generated month report view
 */
public class GeneratedReportActivity extends AppCompatActivity implements ValueEventListener {
    private static final String TAG = "GeneratedReportActivity";
    public static final String USER_EXTRA = "user_extra";
    public static final String YEAR_EXTRA = "year_extra";
    public static final String MONTH_EXTRA = "month_extra";

    private TextView mUserName;
    private TextView mDNIValue;
    private TextView mDateValue;
    private TextView mHoursValue;
    private RecyclerView mRecyclerView;

    private String mYear;
    private String mMonth;
    private User mUser;
    private Long mWorktime;

    private RegistryManager mRegistryManager;
    private EventManager mEventManager;
    private ReportListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generated_report);

        // Set title
        setTitle(getString(R.string.title_activity_generated_report));

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Initialize database managers
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRegistryManager = new RegistryManager(database);
        mEventManager = new EventManager(database);
        mRegistryManager.addValueEventListener(this);
        mEventManager.addValueEventListener(this);

        // Load view components
        mUserName = findViewById(R.id.userName);
        mDNIValue = findViewById(R.id.dniValue);
        mDateValue = findViewById(R.id.dateValue);
        mHoursValue = findViewById(R.id.hoursValue);
        mRecyclerView = findViewById(R.id.report_list_recycler_view);

        // Load intent extra parameters
        mUser = (User) getIntent().getSerializableExtra(USER_EXTRA);
        mYear = getIntent().getStringExtra(YEAR_EXTRA);
        mMonth = getIntent().getStringExtra(MONTH_EXTRA);

        // Specify an adapter
        adapter = new ReportListAdapter(this, mYear, mMonth, mUser.getId());
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Set header values
        mUserName.setText(mUser.getName() + " \n" + mUser.getSurname1() + " " + mUser.getSurname2());
        mDNIValue.setText(mUser.getId());
        mDateValue.setText(mMonth + " " + mYear);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.share_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                try {
                    // For shared action, generates a PDF document report
                    String path = ReportUtils.generateReport(mUser, mYear, mMonth, mWorktime,
                            adapter.getDaysWorktime(), adapter.getEntranceTime(), adapter.getExitTime());

                    Log.d(TAG, path);

                    // Send document through a chooser app intent.
                    Intent shareIntent = new Intent();
                    File file = new File(path);
                    if (file.exists()) {
                        shareIntent.setType("application/pdf");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + path));
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing File...");
                        startActivity(Intent.createChooser(shareIntent, "Share File"));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return true;
    }


    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        // Recalculates worked time and show in text view
        WorkingHourUtils workingHoursUtil = new WorkingHourUtils(mRegistryManager, mEventManager);
        mWorktime = workingHoursUtil.calculateWorkTime(mYear, mMonth, mUser.getId());
        int seconds = (int) (mWorktime / 1000) % 60;
        int minutes = (int) ((mWorktime / (1000 * 60)) % 60);
        int hours = (int) ((mWorktime / (1000 * 60 * 60)));
        String secondsString = seconds < 10 ? "0" + seconds : String.valueOf(seconds);
        String minutesString = minutes < 10 ? "0" + minutes : String.valueOf(minutes);
        mHoursValue.setText(hours + ":" + minutesString + ":" + secondsString);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
