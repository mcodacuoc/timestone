package uoc.edu.timestone.ui.system;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.CenterListAdapter;
import uoc.edu.timestone.adapter.EventListAdapter;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Event;

public class SystemConfigActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private CenterListAdapter mCenterListAdapter;
    private EventListAdapter mEventListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_config);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load center list recycler view
        recyclerView = findViewById(R.id.center_list_recycler_view);

        // Specify an adapter
        mCenterListAdapter = new CenterListAdapter(this);
        recyclerView.setAdapter(mCenterListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Load center list recycler view
        recyclerView = findViewById(R.id.event_list_recycler_view);

        // Specify an adapter
        mEventListAdapter= new EventListAdapter(this);
        recyclerView.setAdapter(mEventListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton addCenterButton = findViewById(R.id.addCenterActionButton);
        addCenterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(v.getContext(), CenterEditActivity.class);
                activityIntent.putExtra(CenterEditActivity.EDIT_MODE, false);
                startActivityForResult(activityIntent, CenterEditActivity.NEW_CENTER_REQUEST);
            }
        });

        FloatingActionButton addEventButton = findViewById(R.id.addEventActionButton);
        addEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(v.getContext(), EventEditActivity.class);
                activityIntent.putExtra(EventEditActivity.EDIT_MODE, false);
                startActivityForResult(activityIntent, EventEditActivity.NEW_EVENT_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Make sure the request was successful
        if (resultCode == RESULT_OK) {
            if (requestCode == CenterEditActivity.NEW_CENTER_REQUEST) {
                // For new contact request, calls user manager add method
                Center center = (Center) data.getSerializableExtra(CenterEditActivity.SELECTED_CENTER);
                mCenterListAdapter.addCenter(center);

            } else if (requestCode == CenterEditActivity.EDIT_CENTER_REQUEST) {
                // For remove or update contact request, calls user manager appropriate method
                Center center = (Center) data.getSerializableExtra(CenterEditActivity.SELECTED_CENTER);
                boolean remove = data.getBooleanExtra(CenterEditActivity.REMOVE_ACTION, false);

                if (remove) {
                    mCenterListAdapter.removeCenter(center);
                } else {
                    mCenterListAdapter.updateCenter(center);
                }
            }
            else if (requestCode == EventEditActivity.NEW_EVENT_REQUEST) {
                // For new contact request, calls user manager add method
                Event event = (Event) data.getSerializableExtra(EventEditActivity.SELECTED_EVENT);
                mEventListAdapter.addEvent(event);

            } else if (requestCode == EventEditActivity.EDIT_EVENT_REQUEST) {
                // For remove or update contact request, calls user manager appropriate method
                Event event = (Event) data.getSerializableExtra(EventEditActivity.SELECTED_EVENT);
                boolean remove = data.getBooleanExtra(EventEditActivity.REMOVE_ACTION, false);

                if (remove) {
                    mEventListAdapter.removeEvent(event);
                } else {
                    mEventListAdapter.updateEvent(event);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
