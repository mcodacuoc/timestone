package uoc.edu.timestone.ui.stats;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Company;
import uoc.edu.timestone.model.User;

/**
 * This class implements the analysis query window.
 */
public class AnalysisFragment extends Fragment {
    private UserManager mUserManager;
    private CenterManager mCenterManager;
    private View root;

    Spinner mSelectCenter;
    Spinner mSelectUser;
    EditText mFromDateText;
    EditText mUntilDateText;
    Button mGenerateButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Initialize database managers
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mUserManager = new UserManager(database);
        mCenterManager = new CenterManager(database);

        // Load root view
        root = inflater.inflate(R.layout.fragment_analysis, container, false);

        // Load view content elements
        mSelectCenter = root.findViewById(R.id.selectCenter);
        mSelectUser = root.findViewById(R.id.selectUser);
        mFromDateText = root.findViewById(R.id.fromDateEditText);
        mUntilDateText = root.findViewById(R.id.untilDateEditText);
        mGenerateButton = root.findViewById(R.id.generateButton);

        // If user its not RRHH responsible, select user will not be available
        if (!MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
            mSelectUser.setVisibility(View.GONE);
            root.findViewById(R.id.userSelectTitle).setVisibility(View.GONE);
        }

        // Load users as model for user selector
        mCenterManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Center> centerList = mCenterManager.getCenters();
                centerList.add(0, new Center("", "Todos", "", "",
                        0, new Company("", "")));
                mSelectCenter.setAdapter(new ArrayAdapter<>(
                        root.getContext(), android.R.layout.simple_spinner_dropdown_item, centerList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // Load enters as model for center selector
        mUserManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<User> userList = mUserManager.getUsers();
                userList.add(0, new User(
                        "", "", "", "Todos",
                        "", "", "", 0));
                mSelectUser.setAdapter(new ArrayAdapter<>(
                        root.getContext(), android.R.layout.simple_spinner_dropdown_item, userList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Set listener for click button event
        mGenerateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Text fields shall be filled
                if (mFromDateText.getText().toString().isEmpty() ||
                        mUntilDateText.getText().toString().isEmpty()) {
                    Snackbar.make(v, getString(R.string.empty_values_error),
                            Snackbar.LENGTH_SHORT).show();
                } else {
                    // Start chart result activity with selected information
                    Intent activityIntent = new Intent(root.getContext(), AnalysisResultActivity.class);
                    activityIntent.putExtra(AnalysisResultActivity.FROM_DATE_EXTRA,
                            mFromDateText.getText().toString());
                    activityIntent.putExtra(AnalysisResultActivity.UNTIL_DATE_EXTRA,
                            mUntilDateText.getText().toString());
                    activityIntent.putExtra(AnalysisResultActivity.CENTER_EXTRA,
                            mCenterManager.getCenters().get(mSelectCenter.getSelectedItemPosition()));

                    if (!MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
                        activityIntent.putExtra(AnalysisResultActivity.USER_EXTRA,
                                MainEmptyActivity.mCurrentUser);
                    } else {
                        activityIntent.putExtra(AnalysisResultActivity.USER_EXTRA,
                                mUserManager.getUsers().get(mSelectUser.getSelectedItemPosition()));
                    }
                    startActivity(activityIntent);
                }
            }
        });

        return root;
    }
}
