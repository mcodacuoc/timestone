package uoc.edu.timestone.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import uoc.edu.timestone.MainActivity;
import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.User;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.FirebaseDatabase;

/**
 * This is the application LoginActivity
 */
public class LoginActivity extends AppCompatActivity {
    protected static final String TAG = "LoginActivity";

    private UserManager mUserManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Timestone");

        // Initialize UserManager
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mUserManager = new UserManager(database);

        // Load view elements
        final TextView emailTextView = findViewById(R.id.emailEditText);
        final TextView passwordTextView = findViewById(R.id.passwordEditText);
        Button loginButton = findViewById(R.id.loginButton);

        // Add an onClickEvent listener for login button
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailTextView.getText().toString();
                String password = passwordTextView.getText().toString();

                User user = mUserManager.getUserByEmail(email);

                // If user and password are equivalent to the stored in database
                // then starts main activity and stores the login data in shared
                // preferences
                if ((null != user) &&
                        (user.getPassword().equals(password))) {
                    // select your mode to be either private or public.
                    int mode = Activity.MODE_PRIVATE;

                    // get the sharedPreference of your context.
                    SharedPreferences mySharedPreferences;
                    mySharedPreferences = getSharedPreferences("timestone_login", mode);

                    // retrieve an editor to modify the shared preferences
                    SharedPreferences.Editor editor = mySharedPreferences.edit();

                    // now store your primitive type values.
                    editor.putString("email", email);
                    editor.putString("password", password);

                    //save the changes that you made
                    editor.commit();

                    MainEmptyActivity.mCurrentUser = user;

                    // Launch main activity
                    Intent activityIntent = new Intent(v.getContext(), MainActivity.class);
                    startActivity(activityIntent);
                    finish();
                } else {
                    Snackbar.make(findViewById(R.id.loginButton),
                            "El usuario o la contraseña son incorrectos", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
            }
        });
    }
}
