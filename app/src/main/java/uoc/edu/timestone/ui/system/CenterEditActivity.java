package uoc.edu.timestone.ui.system;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.CompanyManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Company;

public class CenterEditActivity extends AppCompatActivity {
    public static final String EDIT_MODE = "edit";
    public static final String SELECTED_CENTER = "center";
    public static final String REMOVE_ACTION = "remove_flag";
    public static final int NEW_CENTER_REQUEST = 1;
    public static final int EDIT_CENTER_REQUEST = 2;

    private boolean isEdit = false;
    private Center mCenter = null;
    private CompanyManager mCompanyManager;

    private EditText mNameEdit;
    private EditText mAddressEdit;
    private EditText mCityEdit;
    private EditText mPostalCodeEdit;
    private Spinner mCompanySelect;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_center_edit);

        mContext = this;

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load activity control references
        mNameEdit = findViewById(R.id.centerNameEdit);
        mAddressEdit = findViewById(R.id.centerAddressEdit);
        mCityEdit = findViewById(R.id.centerCityEdit);
        mPostalCodeEdit = findViewById(R.id.centerPostalCodeEdit);
        mCompanySelect = (Spinner) findViewById(R.id.companySelect);

        // Company database value change will reload company select spinner
        mCompanyManager = new CompanyManager(FirebaseDatabase.getInstance());
        mCompanyManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCompanySelect.setAdapter(new ArrayAdapter<>(
                        mContext, android.R.layout.simple_spinner_dropdown_item,
                        mCompanyManager.getCompanies()));

                if (null != mCenter) {
                    for (int i = 0; i < mCompanyManager.getCompanies().size(); i++) {
                        if (mCompanyManager.getCompanies().get(i).getId().equals(mCenter.getCompanyId())) {
                            mCompanySelect.setSelection(i);
                            break;
                        }
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Get edition intent parameter
        isEdit = getIntent().getBooleanExtra(EDIT_MODE, false);

        // If activity have been opened in edition mode, then center information is loaded.
        if (isEdit) {
            // The activity title shall be edit
            setTitle(R.string.title_activity_edit);
            mCenter = (Center) getIntent().getSerializableExtra(SELECTED_CENTER);
            mNameEdit.setText(mCenter.getName());
            mAddressEdit.setText(mCenter.getAddress());
            mCityEdit.setText(mCenter.getCity());
            mPostalCodeEdit.setText(String.valueOf(mCenter.getCp()));
        } else {
            // In creation mode, title shall be new
            setTitle(R.string.title_activity_new);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        if (!isEdit) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent returnIntent = new Intent();
        switch (item.getItemId()) {
            // If save action have been performed, the activity will finish returning
            // the edited or created center
            case R.id.action_save:
                if (saveCenter()) {
                    returnIntent.putExtra(REMOVE_ACTION, false);
                    returnIntent.putExtra(SELECTED_CENTER, mCenter);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Snackbar.make(this.findViewById(android.R.id.content),
                            getString(R.string.empty_values_error),
                            Snackbar.LENGTH_SHORT).show();
                }
                return true;

            //  If action performed was delete, then selected center will be returned
            case R.id.action_delete:
                returnIntent.putExtra(REMOVE_ACTION, true);
                returnIntent.putExtra(SELECTED_CENTER, mCenter);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method saves introduced data in a new or edited center.
     *
     * @return <code>true</code> if information is valid.
     */
    private boolean saveCenter() {
        boolean result = false;
        if (isValid()) {
            if (!isEdit) {
                mCenter = new Center();
            }
            mCenter.setName(mNameEdit.getText().toString());
            mCenter.setAddress(mAddressEdit.getText().toString());
            mCenter.setCity(mCityEdit.getText().toString());
            mCenter.setCp(Integer.parseInt(mPostalCodeEdit.getText().toString()));
            Company company = mCompanyManager.getCompanies().get(mCompanySelect.getSelectedItemPosition());
            mCenter.setCompanyId(company.getId());

            result = true;
        }
        return result;
    }

    /**
     * This method tells wheter introduced information os valid
     *
     * @return <code>true</code> if information is valid
     */
    private boolean isValid() {
        boolean result = true;
        if (0 == mNameEdit.length() || 0 == mAddressEdit.length() ||
                0 == mCityEdit.length() || 0 == mPostalCodeEdit.length()) {
            result = false;
        }
        return result;
    }
}
