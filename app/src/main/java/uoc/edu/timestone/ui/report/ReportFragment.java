package uoc.edu.timestone.ui.report;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.utils.DateTimeUtils;
import uoc.edu.timestone.utils.WorkingHourUtils;

/**
 * This class implements the report menu activity window
 */
public class ReportFragment extends Fragment {
    private UserManager mUserManager;
    private View root;

    EditText mSelectYear;
    Spinner mSelectMonth;
    Spinner mSelectUser;
    Button mGenerateButton;
    WorkingHourUtils mWorkingHourUtils;
    RegistryManager mRegistryManager;
    EventManager mEventManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Initialize database managers
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mUserManager = new UserManager(database);
        mRegistryManager = new RegistryManager(database);
        mEventManager = new EventManager(database);

        mWorkingHourUtils = new WorkingHourUtils(mRegistryManager, mEventManager);

        // Load root view
        root = inflater.inflate(R.layout.fragment_report, container, false);

        // Load view content elements
        mSelectYear = root.findViewById(R.id.selectYear);
        mSelectMonth = root.findViewById(R.id.selectMonth);
        mSelectUser = root.findViewById(R.id.selectUser);
        mGenerateButton = root.findViewById(R.id.generateButton);

        // By default, set current year
        mSelectYear.setText(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));

        if (!MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
            mSelectUser.setVisibility(View.GONE);
            root.findViewById(R.id.selectUserTitle).setVisibility(View.GONE);
        }

        // Load month strings as selector values
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(root.getContext(),
                R.array.month_values_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSelectMonth.setAdapter(adapter);

        // Load users as model for user selector
        mUserManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<User> userList =
                        mUserManager.getUsers();
                mSelectUser.setAdapter(new ArrayAdapter<>(
                        root.getContext(), android.R.layout.simple_spinner_dropdown_item, userList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Set on click listener for generate button
        mGenerateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user;
                if (!MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
                    user = MainEmptyActivity.mCurrentUser;
                } else {
                    user = mUserManager.getUsers().get(mSelectUser.getSelectedItemPosition());
                }

                if (mSelectYear.getText().toString().isEmpty()) {
                    Snackbar.make(v, getString(R.string.empty_values_error),
                            Snackbar.LENGTH_SHORT).show();
                } else {
                    Intent activityIntent = new Intent(v.getContext(), GeneratedReportActivity.class);
                    activityIntent.putExtra(GeneratedReportActivity.USER_EXTRA, user);
                    activityIntent.putExtra(GeneratedReportActivity.YEAR_EXTRA, String.valueOf(mSelectYear.getText()));
                    activityIntent.putExtra(GeneratedReportActivity.MONTH_EXTRA, mSelectMonth.getSelectedItem().toString());
                    startActivity(activityIntent);

                    int year = Integer.parseInt(String.valueOf(mSelectYear.getText()));
                    int month = WorkingHourUtils.monthValueMap.get(mSelectMonth.getSelectedItem().toString()) - 1;
                    Calendar mycal = new GregorianCalendar(year, month, 1);
                    int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

                    for (int i = 0; i < daysInMonth; i++) {
                        mycal.set(year, month, i + 1, 0, 0, 0);
                        String dateString = DateTimeUtils.toDateString(mycal.getTime());
                        List<Registry> list = mRegistryManager.getRegistriesFor(user.getId(), dateString);
                        mWorkingHourUtils.checkForErrors(list, user, dateString);
                    }
                }
            }
        });

        return root;
    }
}
