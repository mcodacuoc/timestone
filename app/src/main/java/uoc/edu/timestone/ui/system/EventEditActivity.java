package uoc.edu.timestone.ui.system;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import uoc.edu.timestone.R;
import uoc.edu.timestone.model.Event;

public class EventEditActivity extends AppCompatActivity {
    public static final String EDIT_MODE = "edit";
    public static final String SELECTED_EVENT = "event";
    public static final String REMOVE_ACTION = "remove_flag";
    public static final int NEW_EVENT_REQUEST = 3;
    public static final int EDIT_EVENT_REQUEST = 4;

    private boolean isEdit = false;
    private Event mEvent = null;

    private EditText mNameEdit;
    private CheckBox mWorkingCheck;
    private ImageView mColorButton;
    private Spinner mEventTypeSelect;
    private long mSelectedColor = 16777215;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_edit);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load activity control references
        mNameEdit = findViewById(R.id.eventNameEdit);
        mWorkingCheck = findViewById(R.id.workingEventCheckBox);
        mColorButton = findViewById(R.id.eventColorSquare);

        mEventTypeSelect = findViewById(R.id.eventTypeSelect);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.event_types_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mEventTypeSelect.setAdapter(adapter);

        // Get edition intent parameter
        isEdit = getIntent().getBooleanExtra(EDIT_MODE, false);

        // If event have been opened in edition mode, then event information is loaded.
        if (isEdit) {
            // The activity title shall be edit
            setTitle(R.string.title_activity_edit);
            mEvent = (Event) getIntent().getSerializableExtra(SELECTED_EVENT);
            mNameEdit.setText(mEvent.getName());
            mWorkingCheck.setChecked(mEvent.isWorking());
            mSelectedColor = Long.parseLong(mEvent.getColor().substring(1), 16);
            mColorButton.setBackgroundColor((int) mSelectedColor);

            if (mEvent.getType().equals(Event.EventType.EVENT_TYPE_ENTER)) {
                mEventTypeSelect.setSelection(0);
            } else {
                mEventTypeSelect.setSelection(1);
            }

        } else {
            // In creation mode, title shall be new
            setTitle(R.string.title_activity_new);
        }

        mColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerDialogBuilder
                        .with(v.getContext())
                        .setTitle(getString(R.string.color_event_text_hint))
                        .setColorEditTextColor((int) mSelectedColor)
                        .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                        .showAlphaSlider(false)
                        .showLightnessSlider(false)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                                mSelectedColor = selectedColor;
                                mColorButton.setBackgroundColor(selectedColor);
                            }
                        })
                        .setPositiveButton("ok", new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                                mSelectedColor = selectedColor;
                                mColorButton.setBackgroundColor(selectedColor);
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .build()
                        .show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        if (!isEdit) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent returnIntent = new Intent();
        switch (item.getItemId()) {
            // If save action have been performed, the activity will finish returning
            // the edited or created event
            case R.id.action_save:
                if (saveEvent()) {
                    returnIntent.putExtra(REMOVE_ACTION, false);
                    returnIntent.putExtra(SELECTED_EVENT, mEvent);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Snackbar.make(this.findViewById(android.R.id.content),
                            getString(R.string.empty_values_error),
                            Snackbar.LENGTH_SHORT).show();
                }
                return true;

            //  If action performed was delete, then selected event will be returned
            case R.id.action_delete:
                returnIntent.putExtra(REMOVE_ACTION, true);
                returnIntent.putExtra(SELECTED_EVENT, mEvent);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method saves introduced data in a new or edited event.
     *
     * @return <code>true</code> if information is valid.
     */
    private boolean saveEvent() {
        boolean result = false;
        if (isValid()) {
            if (!isEdit) {
                mEvent = new Event();
            }
            mEvent.setName(mNameEdit.getText().toString());
            mEvent.setWorking(mWorkingCheck.isChecked());
            String color = Long.toHexString(mSelectedColor);
            mEvent.setColor("#" + color.substring(color.length() - 8));
            if (mEventTypeSelect.getSelectedItem().toString().equals("Entrada")) {
                mEvent.setType(Event.EventType.EVENT_TYPE_ENTER);
            } else {
                mEvent.setType(Event.EventType.EVENT_TYPE_EXIT);
            }
            result = true;
        }
        return result;
    }

    /**
     * This method tells wheter introduced information os valid
     *
     * @return <code>true</code> if information is valid
     */
    private boolean isValid() {
        boolean result = true;
        if (0 == mNameEdit.length() || 0 == mSelectedColor) {
            result = false;
        }
        return result;
    }
}
