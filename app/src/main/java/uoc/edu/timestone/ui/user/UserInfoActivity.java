package uoc.edu.timestone.ui.user;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;

import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.ui.login.LoginActivity;

/**
 * This class implements user information activity window
 */
public class UserInfoActivity extends AppCompatActivity {
    public static final String SELECTED_USER = "user";

    private AlertDialog.Builder mDialogBuilder;
    private UserManager mUserManager;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Set activity title
        setTitle(R.string.title_activity_user_info);

        mUserManager = new UserManager(FirebaseDatabase.getInstance());

        // Load activity control references
        TextView nameTextView = findViewById(R.id.nameTextView);
        TextView emailTextView = findViewById(R.id.emailTextView);
        TextView dniTextView = findViewById(R.id.dniValue);
        TextView positionTextView = findViewById(R.id.positionValue);
        TextView workhoursTextView = findViewById(R.id.workhoursValue);
        Button changePasswordButton = findViewById(R.id.changePasswordButton);
        Button logoutButton = findViewById(R.id.logoutButton);

        // Get user intent parameter
        mUser = (User) getIntent().getSerializableExtra(SELECTED_USER);

        if(!mUser.getId().equals(MainEmptyActivity.mCurrentUser.getId())) {
            changePasswordButton.setVisibility(View.GONE);
            logoutButton.setVisibility(View.GONE);
        }

        // Fill controls with user information
        nameTextView.setText(mUser.getName() + " " + mUser.getSurname1() + " " + mUser.getSurname2());
        emailTextView.setText(mUser.getEmail());
        dniTextView.setText(mUser.getId());
        positionTextView.setText(mUser.getPosition());
        workhoursTextView.setText(String.valueOf(mUser.getDayhours()));

        createChangePasswordDialog();

        // This button show a dialog to set new password
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogBuilder.show();
            }
        });

        // Set logout button action
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mode = Activity.MODE_PRIVATE;
                SharedPreferences mySharedPreferences;
                mySharedPreferences = getSharedPreferences("timestone_login", mode);

                // Retrieve an editor to modify the shared preferences
                SharedPreferences.Editor editor = mySharedPreferences.edit();

                // Now store your primitive type values.
                editor.putString("email", "");
                editor.putString("password", "");

                //save the changes that you made
                editor.commit();

                // Send a broadcast signal to finish main activity
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                sendBroadcast(broadcastIntent);

                // Start login activity and finish this one
                Intent homeIntent = new Intent(v.getContext(), LoginActivity.class);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);

                finish();
            }
        });
    }

    /**
     * This method creates a dialog progamatically intended to change user password
     */
    public void createChangePasswordDialog() {
        mDialogBuilder = new AlertDialog.Builder(UserInfoActivity.this);
        mDialogBuilder.setTitle(getString(R.string.change_password_dialog_title));

        final EditText oldPass = new EditText(UserInfoActivity.this);
        final EditText newPass = new EditText(UserInfoActivity.this);
        final EditText confirmPass = new EditText(UserInfoActivity.this);

        oldPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        newPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
        confirmPass.setTransformationMethod(PasswordTransformationMethod.getInstance());

        oldPass.setHint(getString(R.string.current_password_text_hint));
        newPass.setHint(getString(R.string.new_password_text_hint));
        confirmPass.setHint(getString(R.string.confirm_password_text_hint));

        LinearLayout ll = new LinearLayout(UserInfoActivity.this);
        ll.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(50, 20, 50, 0);
        ll.addView(oldPass, layoutParams);
        ll.addView(newPass, layoutParams);
        ll.addView(confirmPass, layoutParams);

        mDialogBuilder.setView(ll);

        mDialogBuilder.setPositiveButton(getString(R.string.accept_button_text),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (MainEmptyActivity.mCurrentUser.getPassword().equals(
                                oldPass.getText().toString()) &&
                                newPass.getText().toString().equals(
                                        confirmPass.getText().toString())) {
                            mUser.setPassword(newPass.getText().toString());
                            mUserManager.updateUser(mUser);
                            dialog.dismiss();
                        }
                    }
                });
        mDialogBuilder.setNegativeButton(getString(R.string.cancel_button_text),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
