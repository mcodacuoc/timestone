package uoc.edu.timestone.ui.workday;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.qap.ctimelineview.TimelineRow;
import org.qap.ctimelineview.TimelineViewAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.utils.DateTimeUtils;

/**
 * This class implements the workday fragment view
 */
public class WorkdayFragment extends Fragment implements ValueEventListener, DatePickerDialog.OnDateSetListener {
    private static final String TAG = "WorkdayFragment";
    private RegistryManager mRegistryManager;
    private EventManager mEventmanager;
    private Calendar mCurrentDate;
    private View root;
    private DatePickerDialog mDatePickerDialog;
    private TextView mTitle;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // Initialize database managers
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRegistryManager = new RegistryManager(database);
        mEventmanager = new EventManager(database);
        mRegistryManager.addValueEventListener(this);

        // Get current date
        mCurrentDate = Calendar.getInstance();

        // Load root view
        root = inflater.inflate(R.layout.fragment_workday, container, false);

        // Creates a new date picker dialog
        mDatePickerDialog = new DatePickerDialog(root.getContext(),
                this, mCurrentDate.get(Calendar.YEAR),
                mCurrentDate.get(Calendar.MONTH), mCurrentDate.get(Calendar.DAY_OF_MONTH));

        // Load view content elements
        mTitle = root.findViewById(R.id.workday_tittle);
        Button mRegisterEnterButton = root.findViewById(R.id.registerEnterButton);
        Button mRegisterExitButton = root.findViewById(R.id.registerExitButton);
        Button mEditWorkdayButton = root.findViewById(R.id.editWorkdayButton);
        ImageButton calendarButton = root.findViewById(R.id.calendarButton);

        // By default, title will be the current date
        mTitle.setText("Hoy - " + DateTimeUtils.toDateString(mCurrentDate.getTime()));
        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatePickerDialog.show();
            }
        });

        // Set on click event listener for add enter registry button.
        mRegisterEnterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(root.getContext(), RegistryEditActivity.class);
                activityIntent.putExtra(RegistryEditActivity.EDIT_TYPE, RegistryEditActivity.EditType.NEW_REGISTRY);
                activityIntent.putExtra(RegistryEditActivity.EVENT_TYPE, Event.EventType.EVENT_TYPE_ENTER);
                activityIntent.putExtra(RegistryEditActivity.DATE_EXTRA, DateTimeUtils.toDateString(mCurrentDate.getTime()));
                activityIntent.putExtra(RegistryEditActivity.TIME_EXTRA, DateTimeUtils.toTimeString(mCurrentDate.getTime()));
                startActivity(activityIntent);
            }
        });

        // Set on click event listener for add exit registry button
        mRegisterExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(root.getContext(), RegistryEditActivity.class);
                activityIntent.putExtra(RegistryEditActivity.EDIT_TYPE, RegistryEditActivity.EditType.NEW_REGISTRY);
                activityIntent.putExtra(RegistryEditActivity.EVENT_TYPE, Event.EventType.EVENT_TYPE_EXIT);
                activityIntent.putExtra(RegistryEditActivity.DATE_EXTRA, DateTimeUtils.toDateString(mCurrentDate.getTime()));
                activityIntent.putExtra(RegistryEditActivity.TIME_EXTRA, DateTimeUtils.toTimeString(mCurrentDate.getTime()));
                startActivity(activityIntent);
            }
        });

        // Set on click event listener for edit workday
        mEditWorkdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(root.getContext(), WorkdayEditActivity.class);
                activityIntent.putExtra(WorkdayEditActivity.USER_EXTRA, MainEmptyActivity.mCurrentUser.getId());
                activityIntent.putExtra(WorkdayEditActivity.DATE_EXTRA, DateTimeUtils.toDateString(mCurrentDate.getTime()));
                startActivity(activityIntent);
            }
        });

        return root;
    }

    /**
     * This method creates a visual timeline with the list of registries for selected date
     * and current user
     */
    public void createTimeline(String dateString, String userId) {
        // Create Timeline rows List
        ArrayList<TimelineRow> timelineRowsList = new ArrayList<>();

        int i = 0;
        Date date = mCurrentDate.getTime();

        // Creates a new timeline row for each registry retrieved for selected user and date
        for (Registry registry : mRegistryManager.getRegistriesFor(userId, dateString)) {

            Event event = mEventmanager.getEvent(registry.getEventId());

            if (null != event) {
                // Create new timeline row (Row Id)
                TimelineRow myRow = new TimelineRow(i);

                // To set the row Title
                myRow.setTitle(registry.getTime());
                // To set the row Description
                myRow.setDescription(event.getName());
                // To set row Below Line Color
                myRow.setBackgroundColor(Color.parseColor(event.getColor()));
                // To set the Background Size of the row image in dp
                myRow.setBackgroundSize(40);
                // To set row Below Line Color
                myRow.setBellowLineColor(Color.argb(255, 0, 0, 0));
                // Add the new row to the list
                timelineRowsList.add(myRow);
            }
        }

        // Create the Timeline Adapter
        ArrayAdapter<TimelineRow> myAdapter = new TimelineViewAdapter(
                root.getContext(), 0, timelineRowsList, false);

        // Get the ListView and Bind it with the Timeline Adapter
        ListView myListView = root.findViewById(R.id.timeline_listView);
        myListView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        String dateString = DateTimeUtils.toDateString(mCurrentDate.getTime());
        createTimeline(dateString, MainEmptyActivity.mCurrentUser.getId());
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // Set selected date with value returned by date picker dialog
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, dayOfMonth,
                cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
        mCurrentDate = cal;

        // Updates timeline for new date
        String dateString = DateTimeUtils.toDateString(cal.getTime());
        createTimeline(dateString, MainEmptyActivity.mCurrentUser.getId());

        // Set title with the new selected date
        mTitle.setText("Día - " + DateTimeUtils.toDateString(mCurrentDate.getTime()));
    }
}
