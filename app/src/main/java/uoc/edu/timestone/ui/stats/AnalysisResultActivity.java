package uoc.edu.timestone.ui.stats;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Pair;
import android.view.MenuItem;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.firebase.database.FirebaseDatabase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.PieChartAdapter;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.User;

/**
 * This class implements query results represented as a chart
 */
public class AnalysisResultActivity extends AppCompatActivity implements Observer {
    public static final String FROM_DATE_EXTRA = "from_date_extra";
    public static final String UNTIL_DATE_EXTRA = "until_date_extra";
    public static final String USER_EXTRA = "user_extra";
    public static final String CENTER_EXTRA = "center_extra";

    private PieChart chart;
    private PieChartAdapter mChartAdapter;
    private EventManager mEventManager;
    private String mFromDate;
    private String mUntilDate;
    private User mUser;
    private Center mCenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis_result);

        // Set title
        setTitle(getString(R.string.title_activity_analysis_result));

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Initialize database managers
        mEventManager = new EventManager(FirebaseDatabase.getInstance());

        // Retrieve intent data
        mFromDate = getIntent().getExtras().getString(FROM_DATE_EXTRA);
        mUntilDate = getIntent().getExtras().getString(UNTIL_DATE_EXTRA);
        mUser = (User) getIntent().getSerializableExtra(USER_EXTRA);
        mCenter = (Center) getIntent().getSerializableExtra(CENTER_EXTRA);

        // Create pie chart
        chart = findViewById(R.id.worktimePieChart);
        createChart();

        // Initialize chart data adapter
        mChartAdapter = new PieChartAdapter(mCenter.getId(), mUser.getId(),
                mFromDate, mUntilDate);

        // Add this class as change values observer
        mChartAdapter.addObserver(this);
    }

    /**
     * This method set all chart visualization parameters
     */
    public void createChart() {
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setDrawHoleEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);
        chart.setDragDecelerationFrictionCoef(0.95f);
        chart.setRotationAngle(0);

        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        chart.animateY(1400, Easing.EaseInOutQuad);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTextSize(12f);
    }

    /**
     * This method updates chart entries and legend data
     *
     * @param timeMap Event/Hours content
     * @param total   Total calculated hours
     */
    private void updateChart(HashMap<String, Pair<Long, Integer>> timeMap, long total) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();
        ArrayList<LegendEntry> legendEntries = new ArrayList<>();

        // Loop through event hour elements
        Iterator<String> itr = timeMap.keySet().iterator();
        while (itr.hasNext()) {

            // Obtains number of hours value and event
            String key = itr.next();
            Long value = timeMap.get(key).first;
            Event event = mEventManager.getEvent(key);

            if (null != event) {

                // Creates a new chart entry value
                if (total != 0) {
                    entries.add(new PieEntry((float) value * 100 / total));
                } else {
                    entries.add(new PieEntry((float) 0));
                }
                // Set chart color for each event
                colors.add(Color.parseColor(event.getColor()));

                // Set a new legend entry values
                int seconds = (int) ((float) value / 1000) % 60;
                int minutes = (int) (((float) value / (1000 * 60)) % 60);
                int hours = (int) (((float) value / (1000 * 60 * 60)));
                String secondsString = seconds < 10 ? "0" + seconds : String.valueOf(seconds);
                String minutesString = minutes < 10 ? "0" + minutes : String.valueOf(minutes);
                String stringValue = (hours + ":" + minutesString + ":" + secondsString);
                String eventName = "";

                if (event.getId().equals(EventManager.ENTER_EVENT_ID)) {
                    eventName = getString(R.string.working_event_name);
                } else {
                    eventName = event.getName();
                }

                legendEntries.add(new LegendEntry(eventName + " " + stringValue + "h",
                        Legend.LegendForm.SQUARE, Float.NaN, Float.NaN,
                        null, Color.parseColor(event.getColor())));
            }
        }

        // Name for the data set
        PieDataSet dataSet = new PieDataSet(entries, "Horas dedicadas");
        dataSet.setDrawIcons(false);
        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        chart.setData(data);

        // Set legend visual parameter
        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXOffset(5f);
        l.setXEntrySpace(10f);
        l.setYEntrySpace(5f);
        l.setYOffset(5f);
        l.setEntries(legendEntries);
        l.setTextSize(14f);

        // Undo all highlights
        chart.highlightValues(null);
        chart.invalidate();
    }

    @Override
    public void update(Observable o, Object arg) {
        updateChart(mChartAdapter.getEntryData().getEventMap(),
                mChartAdapter.getEntryData().getTotalTime());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
