package uoc.edu.timestone.ui.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import uoc.edu.timestone.R;
import uoc.edu.timestone.model.User;

/**
 * This class implements user edit activity window
 */
public class UserEditActivity extends AppCompatActivity {
    public static final String EDIT_MODE = "edit";
    public static final String SELECTED_USER = "user";
    public static final String REMOVE_ACTION = "remove_flag";
    public static final int NEW_CONTACT_REQUEST = 1;
    public static final int EDIT_CONTACT_REQUEST = 2;

    private boolean isEdit = false;
    private User mUser = null;

    private EditText mEmailEdit;
    private EditText mDNIEdit;
    private EditText mNameEdit;
    private EditText mSurname1Edit;
    private EditText mSurname2Edit;
    private EditText mPositionEdit;
    private EditText mWorkhoursEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load activity control references
        mEmailEdit = findViewById(R.id.userEmailEdit);
        mDNIEdit = findViewById(R.id.userDNIEdit);
        mNameEdit = findViewById(R.id.userNameEdit);
        mSurname1Edit = findViewById(R.id.userSurname1Edit);
        mSurname2Edit = findViewById(R.id.userSurname2Edit);
        mPositionEdit = findViewById(R.id.userPositionEdit);
        mWorkhoursEdit = findViewById(R.id.userWorkhoursEdit);

        // Get edition intent parameter
        isEdit = getIntent().getBooleanExtra(EDIT_MODE, false);

        // If activity have been opened in edition mode, then user information is loaded.
        if (isEdit) {
            // The activity title shall be edit
            setTitle(R.string.title_activity_edit);
            mUser = (User) getIntent().getSerializableExtra(SELECTED_USER);
            mEmailEdit.setText(mUser.getEmail());
            mDNIEdit.setText(mUser.getId());
            mNameEdit.setText(mUser.getName());
            mSurname1Edit.setText(mUser.getSurname1());
            mSurname2Edit.setText(mUser.getSurname2());
            mPositionEdit.setText(mUser.getPosition());
            mWorkhoursEdit.setText(String.valueOf(mUser.getDayhours()));

            // Edit DNI its forbidden
            mDNIEdit.setVisibility(View.GONE);

        } else {
            // In creation mode, title shall be new
            setTitle(R.string.title_activity_new);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        if (!isEdit) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent returnIntent = new Intent();
        switch (item.getItemId()) {
            // If save action have been performed, the activity will finish returning
            // the edited or created user
            case R.id.action_save:
                if (saveUser()) {
                    returnIntent.putExtra(REMOVE_ACTION, false);
                    returnIntent.putExtra(SELECTED_USER, mUser);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Snackbar.make(this.findViewById(android.R.id.content),
                            getString(R.string.empty_values_error),
                            Snackbar.LENGTH_SHORT).show();
                }
                return true;

            //  If action performed was delete, then selected user will be returned
            case R.id.action_delete:
                returnIntent.putExtra(REMOVE_ACTION, true);
                returnIntent.putExtra(SELECTED_USER, mUser);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method saves introduced data in a new or edited user.
     *
     * @return <code>true</code> if information is valid.
     */
    private boolean saveUser() {
        boolean result = false;
        if (isValid()) {
            if (!isEdit) {
                mUser = new User();
                mUser.setPassword("password");
            }
            mUser.setId(mDNIEdit.getText().toString());
            mUser.setEmail(mEmailEdit.getText().toString());
            mUser.setName(mNameEdit.getText().toString());
            mUser.setSurname1(mSurname1Edit.getText().toString());
            mUser.setSurname2(mSurname2Edit.getText().toString());
            mUser.setPosition(mPositionEdit.getText().toString());
            mUser.setDayhours(Integer.parseInt(mWorkhoursEdit.getText().toString()));

            result = true;
        }
        return result;
    }

    /**
     * This method tells wheter introduced information os valid
     *
     * @return <code>true</code> if information is valid
     */
    private boolean isValid() {
        boolean result = true;
        if (0 == mEmailEdit.length() || 0 == mDNIEdit.length() || 0 == mNameEdit.length() ||
                0 == mSurname1Edit.length() || 0 == mSurname2Edit.length() ||
                0 == mPositionEdit.length() || 0 == mWorkhoursEdit.length()) {
            result = false;
        }
        return result;
    }
}
