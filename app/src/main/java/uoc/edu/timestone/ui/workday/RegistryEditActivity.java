package uoc.edu.timestone.ui.workday;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.AlertManager;
import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.Alert;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;

import static android.view.View.GONE;

/**
 * This class implements the registry edit activity window
 */
public class RegistryEditActivity extends AppCompatActivity {
    public static final String EVENT_TYPE = "event_type";
    public static final String EDIT_TYPE = "edit_type";
    public static final String NOTIFICATION_EXTRA = "notification_extra";
    public static final String REGISTRY_EXTRA = "registry_extra";
    public static final String DATE_EXTRA = "date_extra";
    public static final String TIME_EXTRA = "time_extra";

    public enum EditType {
        NEW_REGISTRY,
        EDIT_REGISTRY,
        CONFIRM_EVENT
    }

    private EditType mEditType;
    private Event.EventType mEventType;

    private EventManager mEventManager;
    private CenterManager mCenterManager;
    private RegistryManager mRegistryManager;
    private AlertManager mAlertManager;

    private Alert mAlert = null;
    private Registry mRegistry = null;
    private String mDate = null;
    private String mTime = null;

    private EditText mDateEdit;
    private EditText mTimeEdit;
    private Spinner mEventSelect;
    private Spinner mCenterSelect;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);
        mContext = this;

        // Initialize database managers
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mEventManager = new EventManager(database);
        mRegistryManager = new RegistryManager(database);
        mCenterManager = new CenterManager(database);
        mAlertManager = new AlertManager(database);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load view components
        mDateEdit = findViewById(R.id.registryDateEdit);
        mTimeEdit = findViewById(R.id.registryTimeEdit);
        mEventSelect = findViewById(R.id.registryEventSelector);
        mCenterSelect = findViewById(R.id.registryCenterSelector);

        // Add value event listener for events. It will load select event spinner contents
        mEventManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Event> eventList =
                        mEventManager.getEventForType(Event.EventType.EVENT_TYPE_EXIT);
                mEventSelect.setAdapter(new ArrayAdapter<>(
                        mContext, android.R.layout.simple_spinner_dropdown_item, eventList));

                if (mEditType.equals(EditType.EDIT_REGISTRY) && null != mRegistry) {
                    for (int i = 0; i < eventList.size(); i++) {
                        if (eventList.get(i).getId().equals(mRegistry.getEventId())) {
                            mEventSelect.setSelection(i);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Add value event listener for centers. It will load select center spinner contents
        mCenterManager.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCenterSelect.setAdapter(new ArrayAdapter<>(
                        mContext, android.R.layout.simple_spinner_dropdown_item,
                        (mCenterManager.getCenters())));

                if (mEditType.equals(EditType.EDIT_REGISTRY) && null != mRegistry) {
                    for (int i = 0; i < mCenterManager.getCenters().size(); i++) {
                        if (mCenterManager.getCenters().get(i).getId().equals(mRegistry.getCenterId())) {
                            mCenterSelect.setSelection(i);
                            break;
                        }
                    }
                } else if (mEditType.equals(EditType.CONFIRM_EVENT) && null != mAlert) {
                    for (int i = 0; i < mCenterManager.getCenters().size(); i++) {
                        if (mCenterManager.getCenters().get(i).getId().equals(mAlert.getCenterId())) {
                            mCenterSelect.setSelection(i);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Button cancelButton = findViewById(R.id.registryCancelButton);
        Button saveButton = findViewById(R.id.registrySaveButton);

        // Get edition intent parameter
        mEventType = (Event.EventType) getIntent().getSerializableExtra(EVENT_TYPE);
        mEditType = (EditType) getIntent().getSerializableExtra(EDIT_TYPE);

        // If its an enter event type, the type of event selector will not be available
        if (Event.EventType.EVENT_TYPE_ENTER.equals(mEventType)) {
            findViewById(R.id.registryEventSelectorTitle).setVisibility(GONE);
            mEventSelect.setVisibility(GONE);
        }

        // If activity is opened for edit registry, then set the registry values
        if (mEditType.equals(EditType.EDIT_REGISTRY)) {
            setTitle(getString(R.string.title_activity_edit));
            mRegistry = (Registry) getIntent().getSerializableExtra(REGISTRY_EXTRA);
            if (null != mRegistry) {
                mDateEdit.setText(mRegistry.getDate());
                mTimeEdit.setText(mRegistry.getTime());
            }
        }
        // If activity is opened for alert confirmation, the set alert values
        else if (mEditType.equals(EditType.CONFIRM_EVENT)) {
            setTitle(getString(R.string.title_activity_new));
            mAlert = (Alert) getIntent().getSerializableExtra(NOTIFICATION_EXTRA);
            if (null != mAlert) {
                mDateEdit.setText(mAlert.getDate());
                mTimeEdit.setText(mAlert.getTime());
            }
        }
        // For a new registry, set date and time passed as parameters
        else {
            setTitle(getString(R.string.title_activity_new));
            mDate = getIntent().getStringExtra(DATE_EXTRA);
            mTime = getIntent().getStringExtra(TIME_EXTRA);
            mDateEdit.setText(mDate);
            mTimeEdit.setText(mTime);
        }

        // Se cancel button on click listener
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Set save button on click listener
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event event = null;
                // If the event type is exit, set event selected in spinner
                if (mEventType.equals(Event.EventType.EVENT_TYPE_EXIT)) {
                    event = (Event) mEventSelect.getSelectedItem();
                }
                // Otherwise, will be an enter event
                else {
                    event = mEventManager.getEvent(EventManager.ENTER_EVENT_ID);
                }

                // Set new values for edited registry
                if (mEditType.equals(EditType.EDIT_REGISTRY)) {
                    Center center = (Center) mCenterSelect.getSelectedItem();
                    mRegistry.setCenterId(center.getId());
                    mRegistry.setEventId(event.getId());
                    mRegistry.setDate(mDateEdit.getText().toString());
                    mRegistry.setTime(mTimeEdit.getText().toString());
                    mRegistry.setConfirmed(true);
                    mRegistryManager.updateRegistry(mRegistry);

                }
                // If its a new alert, the creates a new one with selected values
                else {
                    if (null != mAlert) {
                        mAlertManager.removeAlert(mAlert);
                    }
                    Center center = (Center) mCenterSelect.getSelectedItem();
                    mRegistryManager.addRegistry(center, event, MainEmptyActivity.mCurrentUser,
                            mDateEdit.getText().toString(),
                            mTimeEdit.getText().toString(), true);
                }
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
