package uoc.edu.timestone.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.NotificationListAdapter;

/**
 * This class implements the notification fragment view
 */
public class NotificationsFragment extends Fragment {
    private RecyclerView recyclerView;
    private NotificationListAdapter adapter;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_notifications, container, false);

        // Load recycler view
        recyclerView = root.findViewById(R.id.notification_list_recycler_view);

        // Specify an adapter (see also next example)
        adapter = new NotificationListAdapter(root.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        return root;
    }
}