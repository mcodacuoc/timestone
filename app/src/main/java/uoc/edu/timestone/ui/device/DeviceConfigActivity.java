package uoc.edu.timestone.ui.device;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.DeviceListAdapter;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.service.DeviceDetectionService;

public class DeviceConfigActivity extends AppCompatActivity {
    private RecyclerView registerDeviceRecyclerView;
    private RecyclerView newDeviceRecyclerView;
    private DeviceListAdapter mRegisteredDeviceListAdapter;
    private DeviceListAdapter mNewDeviceListAdapter;
    private Intent mBeaconDetectionIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBeaconDetectionIntent = new Intent(this, DeviceDetectionService.class);
        startService(mBeaconDetectionIntent);

        setContentView(R.layout.activity_device_config);

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Load devices list recycler views
        registerDeviceRecyclerView = findViewById(R.id.register_device_list_recycler_view);
        newDeviceRecyclerView = findViewById(R.id.new_device_list_recycler_view);

        // Specify an adapter for registered devices
        mRegisteredDeviceListAdapter =
                new DeviceListAdapter(this, DeviceListAdapter.ModeFlag.REGISTERED);
        registerDeviceRecyclerView.setAdapter(mRegisteredDeviceListAdapter);
        registerDeviceRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mNewDeviceListAdapter = new DeviceListAdapter(this,
                DeviceListAdapter.ModeFlag.DETECTED);
        newDeviceRecyclerView.setAdapter(mNewDeviceListAdapter);
        newDeviceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(mBeaconDetectionIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Make sure the request was successful
        if (resultCode == RESULT_OK) {
            if (requestCode == DeviceEditActivity.NEW_DEVICE_REQUEST) {
                // For new contact request, calls user manager add method
                Device device = (Device) data.getSerializableExtra(DeviceEditActivity.SELECTED_DEVICE);
                mRegisteredDeviceListAdapter.addDevice(device);

            } else if (requestCode == DeviceEditActivity.EDIT_DEVICE_REQUEST) {
                // For remove or update contact request, calls user manager appropriate method
                Device device = (Device) data.getSerializableExtra(DeviceEditActivity.SELECTED_DEVICE);
                boolean remove = data.getBooleanExtra(DeviceEditActivity.REMOVE_ACTION, false);

                if (remove) {
                    mRegisteredDeviceListAdapter.removeDevice(device);
                } else {
                    mRegisteredDeviceListAdapter.updateDevice(device);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
