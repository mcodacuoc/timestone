package uoc.edu.timestone.ui.workday;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.adapter.RegistryListAdapter;

/**
 * This class implements the workday edit activity
 */
public class WorkdayEditActivity extends AppCompatActivity {
    public static final String DATE_EXTRA = "date_extra";
    public static final String USER_EXTRA = "user_extra";

    private RecyclerView mRecyclerView;
    private RegistryListAdapter mRegistryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_workday);

        setTitle(getString(R.string.title_activity_workday_edit));

        // Add activity toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Get the date and user parameters needed
        String dateString = getIntent().getStringExtra(DATE_EXTRA);
        String userId = getIntent().getStringExtra(USER_EXTRA);

        // Load devices list recycler views
        mRecyclerView = findViewById(R.id.registryList);

        // Specify an adapter for registered devices
        mRegistryListAdapter =
                new RegistryListAdapter(this, userId, dateString);
        mRecyclerView.setAdapter(mRegistryListAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
