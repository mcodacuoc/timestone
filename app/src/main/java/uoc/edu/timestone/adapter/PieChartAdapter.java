package uoc.edu.timestone.adapter;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Observer;

import androidx.annotation.NonNull;
import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.utils.EntryData;
import uoc.edu.timestone.utils.WorkingHourUtils;

/**
 * This class implements an adapter fro pie chart data
 */
public class PieChartAdapter implements ValueEventListener {
    private RegistryManager mRegisterManager;
    private EventManager mEventManager;
    private UserManager mUserManager;
    private CenterManager mCenterManager;
    private EntryData mEntryData;

    private String mUserId;
    private String mCenterId;
    private String mFromDate;
    private String mUntilDate;

    /**
     * PieChartAdapter class constructor
     *
     * @param centerId  The center identifier filter
     * @param userId    The user identifier filter
     * @param fromDate  The initial date filter
     * @param untilDate The end date filter
     */
    public PieChartAdapter(String centerId, String userId, String fromDate, String untilDate) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRegisterManager = new RegistryManager(database);
        mEventManager = new EventManager(database);
        mUserManager = new UserManager(database);
        mCenterManager = new CenterManager(database);
        mRegisterManager.addValueEventListener(this);
        mEventManager.addValueEventListener(this);
        mUserManager.addValueEventListener(this);
        mCenterManager.addValueEventListener(this);

        mEntryData = new EntryData();
        mCenterId = centerId;
        mUserId = userId;
        mFromDate = fromDate;
        mUntilDate = untilDate;
    }

    /**
     * This method adds an observer for event/hours changes
     *
     * @param o The new observer object
     */
    public void addObserver(Observer o) {
        mEntryData.addObserver(o);
    }

    /**
     * Returns the event hours stored info
     *
     * @return The event hours contained information
     */
    public EntryData getEntryData() {
        return mEntryData;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        WorkingHourUtils workingHourUtils = new WorkingHourUtils(mRegisterManager, mEventManager);
        mEntryData.clear();

        // If user and center have not been specified, calculates dedicated hours for each combination.
        if (mUserId.isEmpty() && mCenterId.isEmpty()) {
            for (Center center : mCenterManager.getCenters()) {
                for (User user : mUserManager.getUsers()) {
                    List<Registry> registryList = mRegisterManager.
                            getRegistriesFor(center.getId(), user.getId(), mFromDate, mUntilDate);
                    workingHourUtils.updateChartEntryData(registryList, mEntryData);
                }
            }
        } else if (mUserId.isEmpty()) {
            // If user have not been specified , calculates dedicated hours for each
            for (User user : mUserManager.getUsers()) {
                List<Registry> registryList = mRegisterManager.
                        getRegistriesFor(mCenterId, user.getId(), mFromDate, mUntilDate);
                workingHourUtils.updateChartEntryData(registryList, mEntryData);
            }
        } else if (mCenterId.isEmpty()) {
            // If center have not been specified, calculates dedicated hours for each
            for (Center center : mCenterManager.getCenters()) {
                List<Registry> registryList = mRegisterManager.
                        getRegistriesFor(center.getId(), mUserId, mFromDate, mUntilDate);
                workingHourUtils.updateChartEntryData(registryList, mEntryData);
            }
        } else {
            // Otherwise, calculates dedicated time for specified user and center
            List<Registry> registryList = mRegisterManager.
                    getRegistriesFor(mCenterId, mUserId, mFromDate, mUntilDate);
            workingHourUtils.updateChartEntryData(registryList, mEntryData);
        }
        mEntryData.setUpdated();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
