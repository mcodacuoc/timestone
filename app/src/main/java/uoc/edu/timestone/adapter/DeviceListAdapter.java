package uoc.edu.timestone.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.DeviceManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.service.DeviceDetectionService;
import uoc.edu.timestone.ui.device.DeviceEditActivity;

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.DeviceListViewHolder>
        implements ValueEventListener, Observer {

    public enum ModeFlag {
        DETECTED,
        REGISTERED
    }

    private static final String TAG = "DeviceListAdapter";
    private DeviceManager mDeviceManager;
    private CenterManager mCenterManager;
    private Context mContext;
    private ModeFlag mMode;

    public DeviceListAdapter(Context context, ModeFlag mode) {
        mMode = mode;
        FirebaseDatabase reference = FirebaseDatabase.getInstance();
        mDeviceManager = new DeviceManager(reference);


        if (ModeFlag.REGISTERED == mode) {
            mCenterManager = new CenterManager(reference);
            mDeviceManager.addValueEventListener(this);
            mCenterManager.addValueEventListener(this);
        } else {
            DeviceDetectionService.addObserver(this);
        }
        this.mContext = context;
    }

    public static class DeviceListViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        TextView mCenterValueLine;
        TextView mUUIDValueLine;
        TextView mMajorValueLine;
        TextView mMinorValueLine;
        TextView mDistanceValueLine;
        TextView mDistanceLabelLine;
        ImageButton mEditButton;
        Device mDevice = null;

        /**
         * DeviceListViewHolder class constructor
         *
         * @param itemView
         */
        public DeviceListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView​ = itemView;
            mCenterValueLine = itemView.findViewById(R.id.centerValueTextLine);
            mUUIDValueLine = itemView.findViewById(R.id.uuidValueTextLine);
            mMajorValueLine = itemView.findViewById(R.id.majorValueTextLine);
            mMinorValueLine = itemView.findViewById(R.id.minorValueTextLine);
            mDistanceValueLine = itemView.findViewById(R.id.distanceValueTextLine);
            mDistanceLabelLine = itemView.findViewById(R.id.distanceLabelTextLine);
            mEditButton = itemView.findViewById(R.id.editDeviceButton);
        }
    }

    @NonNull
    @Override
    public DeviceListAdapter.DeviceListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.device_list_item, parent, false);
        DeviceListAdapter.DeviceListViewHolder vh = new DeviceListAdapter.DeviceListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final DeviceListAdapter.DeviceListViewHolder holder, int position) {
        if (ModeFlag.DETECTED == mMode) {
            holder.mDevice = DeviceDetectionService.getDetectedDevices().get(position);
            holder.mCenterValueLine.setText("iBeacon");
            holder.mEditButton.setImageResource(R.drawable.ic_add_black_24dp);
            holder.mDistanceValueLine.setText(String.format("%.2f", holder.mDevice.getDistance()));
        } else {
            holder.mDevice = mDeviceManager.getDevices().get(position);
            Center center = mCenterManager.getCenter(holder.mDevice.getCenterId());
            holder.mDistanceValueLine.setVisibility(View.GONE);
            holder.mDistanceLabelLine.setVisibility(View.GONE);

            if (null != center) {
                holder.mCenterValueLine.setText(center.getName() + " - " +
                        mDeviceManager.getDeviceTypeString(holder.mDevice.getType()));
            }
        }

        holder.mUUIDValueLine.setText(holder.mDevice.getId());
        holder.mMajorValueLine.setText(String.valueOf(holder.mDevice.getMajor()));
        holder.mMinorValueLine.setText(String.valueOf(holder.mDevice.getMinor()));


        // Add action to edit device info on edit button clicked.
        // Selected device will be pass as a parameter to the edit activity
        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ModeFlag.REGISTERED == mMode) {
                    Intent activityIntent = new Intent(mContext, DeviceEditActivity.class);
                    activityIntent.putExtra(DeviceEditActivity.EDIT_MODE, true);
                    activityIntent.putExtra(DeviceEditActivity.SELECTED_DEVICE, holder.mDevice);
                    ((Activity) mContext).startActivityForResult(activityIntent,
                            DeviceEditActivity.EDIT_DEVICE_REQUEST);
                } else {
                    Intent activityIntent = new Intent(mContext, DeviceEditActivity.class);
                    activityIntent.putExtra(DeviceEditActivity.EDIT_MODE, true);
                    activityIntent.putExtra(DeviceEditActivity.SELECTED_DEVICE, holder.mDevice);
                    ((Activity) mContext).startActivityForResult(activityIntent,
                            DeviceEditActivity.NEW_DEVICE_REQUEST);
                }
            }
        });
    }

    /**
     * This method adds a new device to dataset
     *
     * @param device The new device created
     */
    public void addDevice(Device device) {
        Center center = mCenterManager.getCenter(device.getCenterId());
        mDeviceManager.addDevice(device.getId(), device.getMajor(), device.getMinor(),
                device.getType(), center);
    }

    /**
     * This method updates device passed as parameter
     *
     * @param device Device containing modified information
     */
    public void updateDevice(Device device) {
        mDeviceManager.updateDevice(device);
    }

    /**
     * Removes an device from data set
     *
     * @param device Device to be removed
     */
    public void removeDevice(Device device) {
        mDeviceManager.removeDevice(device);
    }

    @Override
    public int getItemCount() {
        if (ModeFlag.DETECTED == mMode) {
            return DeviceDetectionService.getDetectedDevices().size();
        } else {
            return mDeviceManager.getDevices().size();
        }
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
        Log.d(TAG, "onDataChange");
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Error loading device list");
    }

    @Override
    public void update(Observable o, Object arg) {
        this.notifyDataSetChanged();
        Log.d(TAG, "update");
    }
}
