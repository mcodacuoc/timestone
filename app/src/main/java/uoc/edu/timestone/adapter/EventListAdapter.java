package uoc.edu.timestone.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.ui.system.EventEditActivity;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventListViewHolder>
        implements ValueEventListener {
    private static final String TAG = "EventListAdapter";
    private EventManager mEventManager;
    private Context mContext;

    public EventListAdapter(Context context) {
        mEventManager = new EventManager(FirebaseDatabase.getInstance());
        mEventManager.addValueEventListener(this);
        this.mContext = context;
    }

    public static class EventListViewHolder extends RecyclerView.ViewHolder {
        public Event mEvent;
        public View mView;
        TextView mNameTextView;
        ImageButton mEditButton;
        ImageView mColoredSquare;

        /**
         * EventListViewHolder class constructor
         *
         * @param itemView
         */
        public EventListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView​ = itemView;
            mNameTextView = itemView.findViewById(R.id.firstLine);
            mColoredSquare = itemView.findViewById(R.id.eventColor);
            mEditButton = itemView.findViewById(R.id.eventEditButton);
        }
    }

    @NonNull
    @Override
    public EventListAdapter.EventListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.event_list_item, parent, false);
        EventListAdapter.EventListViewHolder vh = new EventListAdapter.EventListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final EventListAdapter.EventListViewHolder holder, int position) {
        final Event event = mEventManager.getEvents().get(position);
        Log.d(TAG, "onDataChange: " + event.getName());
        holder.mNameTextView.setText(event.getName());
        holder.mColoredSquare.setBackgroundColor(Color.parseColor(event.getColor()));

        if (event.getId().equals(EventManager.ENTER_EVENT_ID) ||
                event.getId().equals(EventManager.END_EVENT_ID)) {
            holder.mEditButton.setVisibility(View.GONE);
        } else {
            // Add action to edit event info on edit button clicked.
            // Selected event will be pass as a parameter to the edit activity
            holder.mEditButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent activityIntent = new Intent(mContext, EventEditActivity.class);
                    activityIntent.putExtra(EventEditActivity.EDIT_MODE, true);
                    activityIntent.putExtra(EventEditActivity.SELECTED_EVENT, event);
                    ((Activity) mContext).startActivityForResult(activityIntent,
                            EventEditActivity.EDIT_EVENT_REQUEST);
                }
            });
        }
    }

    /**
     * This method adds a new event to dataset
     *
     * @param event The new event created
     */
    public void addEvent(Event event) {
        mEventManager.addEvent(event.getName(), event.isWorking(),
                event.getType(), event.getColor());
    }

    /**
     * This method updates event passed as parameter
     *
     * @param event Event containing modified information
     */
    public void updateEvent(Event event) {
        mEventManager.updateEvent(event);
    }

    /**
     * Removes an event from data set
     *
     * @param event Event to be removed
     */
    public void removeEvent(Event event) {
        mEventManager.removeEvent(event);
    }

    @Override
    public int getItemCount() {
        return mEventManager.getEvents().size();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
        Log.d(TAG, "onDataChange");
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Error loading event list");
    }
}
