package uoc.edu.timestone.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.AlertManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Alert;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.ui.workday.RegistryEditActivity;

/**
 * This class implements the notification list view adapter
 */
public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.NotificationListViewHolder>
        implements ValueEventListener {

    private static final String TAG = "NotificationListAdapter";
    private AlertManager mAlertManager;
    private UserManager mUserManager;
    private Context mContext;

    /**
     * NotificationListAdapter class constructor
     *
     * @param context Activity context
     */
    public NotificationListAdapter(Context context) {
        FirebaseDatabase reference = FirebaseDatabase.getInstance();
        mAlertManager = new AlertManager(reference);
        mUserManager = new UserManager(reference);
        mAlertManager.addValueEventListener(this);
        this.mContext = context;
    }

    public static class NotificationListViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        TextView mFirstLine;
        TextView mSecondLine;
        TextView mThirdLine;
        ImageButton mRemoveButton;

        /**
         * DeviceListViewHolder class constructor
         *
         * @param itemView
         */
        public NotificationListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView​ = itemView;
            mFirstLine = itemView.findViewById(R.id.firstLine);
            mSecondLine = itemView.findViewById(R.id.secondLine);
            mThirdLine = itemView.findViewById(R.id.thirdLine);
            mRemoveButton = itemView.findViewById(R.id.removeNotificationButton);
        }
    }

    @NonNull
    @Override
    public NotificationListAdapter.NotificationListViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.notification_list_item, parent, false);
        NotificationListAdapter.NotificationListViewHolder vh =
                new NotificationListAdapter.NotificationListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationListAdapter.NotificationListViewHolder holder, int position) {
        // For RRHH user, only error notifications will be shown
        if (MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
            final Alert alert = mAlertManager.getAlertsForType(
                    Alert.AlertType.ALERT_TYPE_ERROR).get(position);

            if (alert.getType().equals(Alert.AlertType.ALERT_TYPE_ERROR)) {
                User user = mUserManager.getUser(alert.getUserId());
                holder.mSecondLine.setText(mContext.getString(R.string.notification_error_title));
                holder.mThirdLine.setText(mContext.getString(R.string.notification_error_description) +
                        " " + user.getName() + " " + user.getSurname1() + " " +
                        user.getSurname2() + " el " + alert.getDate());
            }
            // Set remove notification button on click listener
            holder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAlertManager.removeAlert(alert);
                }
            });
            // Set process notification button on click listener
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent activityIntent = new Intent(mContext, RegistryEditActivity.class);
                    activityIntent.putExtra(RegistryEditActivity.EDIT_TYPE, RegistryEditActivity.EditType.CONFIRM_EVENT);
                    activityIntent.putExtra(RegistryEditActivity.EVENT_TYPE, Event.EventType.EVENT_TYPE_EXIT);
                    activityIntent.putExtra(RegistryEditActivity.NOTIFICATION_EXTRA, alert);
                    mContext.startActivity(activityIntent);
                }
            });
        } else { // For regular users, their own alerts will be shown
            final Alert alert = mAlertManager.getAlertsForUser(
                    MainEmptyActivity.mCurrentUser.getId()).get(position);
            holder.mFirstLine.setText(alert.getDate());

            if (alert.getType().equals(Alert.AlertType.ALERT_TYPE_ENTER)) {
                holder.mSecondLine.setText(mContext.getString(R.string.notification_enter_title));
                holder.mThirdLine.setText(mContext.getString(R.string.notification_enter_description)
                        + " " + alert.getTime());
            } else if (alert.getType().equals(Alert.AlertType.ALERT_TYPE_EXIT)) {
                holder.mSecondLine.setText(mContext.getString(R.string.notification_exit_title));
                holder.mThirdLine.setText(mContext.getString(R.string.notification_exit_description)
                        + " " + alert.getTime());
            } else if (alert.getType().equals(Alert.AlertType.ALERT_TYPE_ERROR)) {
                User user = mUserManager.getUser(alert.getUserId());
                holder.mSecondLine.setText(mContext.getString(R.string.notification_error_title));
                holder.mThirdLine.setText(mContext.getString(R.string.notification_error_description) +
                        " " + user.getName() + " " + user.getSurname1() + " " +
                        user.getSurname2() + " el " + alert.getDate());
            }
            // Set remove notification button on click listener
            holder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAlertManager.removeAlert(alert);
                }
            });
            // Set process notification button on click listener
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent activityIntent = new Intent(mContext, RegistryEditActivity.class);
                    activityIntent.putExtra(RegistryEditActivity.EDIT_TYPE, RegistryEditActivity.EditType.CONFIRM_EVENT);
                    if (alert.getType().equals(Alert.AlertType.ALERT_TYPE_ENTER)) {
                        activityIntent.putExtra(RegistryEditActivity.EVENT_TYPE, Event.EventType.EVENT_TYPE_ENTER);
                    } else {
                        activityIntent.putExtra(RegistryEditActivity.EVENT_TYPE, Event.EventType.EVENT_TYPE_EXIT);
                    }
                    activityIntent.putExtra(RegistryEditActivity.NOTIFICATION_EXTRA, alert);
                    mContext.startActivity(activityIntent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (MainEmptyActivity.mCurrentUser.getPosition().equals("RRHH")) {
            return mAlertManager.getAlertsForType(Alert.AlertType.ALERT_TYPE_ERROR).size();
        } else {
            return mAlertManager.getAlertsForUser(MainEmptyActivity.mCurrentUser.getId()).size();
        }
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
        Log.d(TAG, "onDataChange");
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Error loading device list");
    }

}
