package uoc.edu.timestone.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.ui.workday.RegistryEditActivity;

/**
 * This class implements an adapter for registry list view
 */
public class RegistryListAdapter extends RecyclerView.Adapter<RegistryListAdapter.RegistryListViewHolder>
        implements ValueEventListener {
    private static final String TAG = "RegistryListAdapter";
    private RegistryManager mRegistryManager;
    private EventManager mEventManager;
    private Context mContext;
    private String mDateString;
    private String mUserId;

    /**
     * RegistryListAdapter class constructor
     *
     * @param context    Activity context
     * @param userId     The user identifier for which records will be displayed
     * @param dateString The day for which records will be displayed
     */
    public RegistryListAdapter(Context context, String userId, String dateString) {
        mRegistryManager = new RegistryManager(FirebaseDatabase.getInstance());
        mEventManager = new EventManager(FirebaseDatabase.getInstance());
        mRegistryManager.addValueEventListener(this);
        this.mContext = context;
        this.mUserId = userId;
        this.mDateString = dateString;
    }

    /**
     * This class implements the registry list view holder
     */
    public static class RegistryListViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        TextView mFirstLineText;
        ImageButton mEditButton;
        ImageButton mRemoveButton;
        ImageView mColoredSquare;

        /**
         * RegistryListViewHolder class constructor
         *
         * @param itemView
         */
        public RegistryListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView​ = itemView;
            mFirstLineText = itemView.findViewById(R.id.firstLine);
            mColoredSquare = itemView.findViewById(R.id.registryColor);
            mEditButton = itemView.findViewById(R.id.registryEditButton);
            mRemoveButton = itemView.findViewById(R.id.registryRemoveButton);
        }
    }

    @NonNull
    @Override
    public RegistryListAdapter.RegistryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.registry_list_item, parent, false);
        RegistryListAdapter.RegistryListViewHolder vh = new RegistryListAdapter.RegistryListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final RegistryListAdapter.RegistryListViewHolder holder, int position) {
        // Get only registries for specified user and date
        final Registry registry = mRegistryManager.getRegistriesFor(mUserId, mDateString).get(position);

        // Get registered event
        final Event event = mEventManager.getEvent(registry.getEventId());

        // Set text line values and color
        holder.mFirstLineText.setText(registry.getTime() + " - " + event.getName());
        holder.mColoredSquare.setBackgroundColor(Color.parseColor(event.getColor()));

        // Set on click listener for edit button that will shows registry edit activity
        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(mContext, RegistryEditActivity.class);
                activityIntent.putExtra(RegistryEditActivity.EDIT_TYPE, RegistryEditActivity.EditType.EDIT_REGISTRY);
                activityIntent.putExtra(RegistryEditActivity.EVENT_TYPE, event.getType());
                activityIntent.putExtra(RegistryEditActivity.REGISTRY_EXTRA, registry);
                ((Activity) mContext).startActivity(activityIntent);
            }
        });

        // Set on click listener for remove registry button
        holder.mRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRegistryManager.removeRegistry(registry);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mRegistryManager.getRegistriesFor(mUserId, mDateString).size();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
        Log.d(TAG, "onDataChange");
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Error loading event list");
    }
}
