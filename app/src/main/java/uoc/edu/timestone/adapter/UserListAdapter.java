package uoc.edu.timestone.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.ui.user.UserEditActivity;
import uoc.edu.timestone.ui.user.UserInfoActivity;

/**
 * This class implements a RecyclerView adapter for users list activity
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserListViewHolder>
        implements ValueEventListener {
    private static final String TAG = "UserListAdapter";
    private UserManager mUserManager;
    private Context mContext;

    /**
     * UserListAdapter constructor
     *
     * @param context Parent activity context
     */
    public UserListAdapter(Context context) {
        mUserManager = new UserManager(FirebaseDatabase.getInstance());
        mUserManager.addValueEventListener(this);
        this.mContext = context;
    }

    /**
     * This class provides a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    public static class UserListViewHolder extends RecyclerView.ViewHolder {
        public User mUser;
        public View mView;
        TextView mNameTextView;
        TextView mEmailTextView;
        TextView mPositionTextView;
        ImageButton mEditButton;

        /**
         * UserListViewHolder class constructor
         *
         * @param itemView
         */
        public UserListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView​ = itemView;
            mNameTextView = itemView.findViewById(R.id.firstLine);
            mEmailTextView = itemView.findViewById(R.id.secondLine);
            mPositionTextView = itemView.findViewById(R.id.thirdLine);
            mEditButton = itemView.findViewById(R.id.editUserButton);
        }
    }

    @NonNull
    @Override
    public UserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.user_list_item, parent, false);
        UserListAdapter.UserListViewHolder vh = new UserListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final UserListViewHolder holder, int position) {
        final User user = mUserManager.getUsers().get(position);
        holder.mNameTextView.setText(user.getName() + " " + user.getSurname1() + " " + user.getSurname2());
        holder.mEmailTextView.setText(user.getEmail());
        holder.mPositionTextView.setText(user.getPosition());

        // Add action to view user info on item clicked
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(mContext, UserInfoActivity.class);
                activityIntent.putExtra(UserInfoActivity.SELECTED_USER, (Serializable) user);
                mContext.startActivity(activityIntent);
            }
        });

        // Add action to edit user info on edit button clicked.
        // Selected user will be pass as a parameter to the edit activity
        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(mContext, UserEditActivity.class);
                activityIntent.putExtra(UserEditActivity.EDIT_MODE, true);
                activityIntent.putExtra(UserEditActivity.SELECTED_USER, user);
                ((Activity) mContext).startActivityForResult(activityIntent,
                        UserEditActivity.EDIT_CONTACT_REQUEST);
            }
        });
    }

    /**
     * This method adds a new user to dataset
     *
     * @param user The new user created
     */
    public void addUser(User user) {
        mUserManager.addUser(user.getId(), user.getEmail(), "password",
                user.getName(), user.getSurname1(), user.getSurname2(), user.getPosition(),
                user.getDayhours());
    }

    /**
     * This method updates user passed as parameter
     *
     * @param user User containing modified information
     */
    public void updateUser(User user) {
        mUserManager.updateUser(user);
    }

    /**
     * Removes an user from data set
     *
     * @param user User to be removed
     */
    public void removeUser(User user) {
        mUserManager.removeUser(user);
    }

    @Override
    public int getItemCount() {
        return mUserManager.getUsers().size();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Error loading user list");
    }
}
