package uoc.edu.timestone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.utils.DateTimeUtils;
import uoc.edu.timestone.utils.WorkingHourUtils;

/**
 * This class implements the report list adapter
 */
public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ReportListViewHolder>
        implements ValueEventListener {

    private static final String TAG = "ReportListAdapter";
    private Context mContext;
    private RegistryManager mRegistryManager;
    private EventManager mEventManager;
    private String mYear;
    private String mMonth;
    private String mUserId;
    private Long[] mDaysWorktime = new Long[31];
    private String[] mEntranceTime = new String[31];
    private String[] mExitTime = new String[31];

    /**
     * ReportListAdapter class constructor
     *
     * @param context The activity context
     * @param year    The year of the report
     * @param month   The month f the report
     * @param userid  The user to obtain report fot
     */
    public ReportListAdapter(Context context, String year, String month, String userid) {
        this.mContext = context;
        this.mYear = year;
        this.mMonth = month;
        this.mUserId = userid;

        // Initialize database managers
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRegistryManager = new RegistryManager(database);
        mEventManager = new EventManager(database);
        mRegistryManager.addValueEventListener(this);
        mEventManager.addValueEventListener(this);
    }

    /**
     * This class implements the view holder for each list element
     */
    public static class ReportListViewHolder extends RecyclerView.ViewHolder {
        TextView mDateValue;
        TextView mEnterValue;
        TextView mExitValue;
        TextView mHoursValue;

        /**
         * ReportListViewHolder class constructor
         *
         * @param itemView
         */
        public ReportListViewHolder(@NonNull View itemView) {
            super(itemView);

            // Load view components
            mDateValue = itemView.findViewById(R.id.dateValue);
            mEnterValue = itemView.findViewById(R.id.enterValue);
            mExitValue = itemView.findViewById(R.id.exitValue);
            mHoursValue = itemView.findViewById(R.id.hoursValue);
        }
    }

    public Long[] getDaysWorktime() {
        return mDaysWorktime;
    }

    public String[] getEntranceTime() {
        return mEntranceTime;
    }

    public String[] getExitTime() {
        return mExitTime;
    }

    @NonNull
    @Override
    public ReportListAdapter.ReportListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.report_list_item, parent, false);
        ReportListAdapter.ReportListViewHolder vh = new ReportListAdapter.ReportListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ReportListAdapter.ReportListViewHolder holder, int position) {
        // Get date string
        int year = Integer.parseInt(mYear);
        int month = WorkingHourUtils.monthValueMap.get(mMonth);
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, position + 1, 0, 0, 0);
        String dateString = DateTimeUtils.toDateString(cal.getTime());
        holder.mDateValue.setText(dateString);

        // Obtain the registry list for specified day and user
        List<Registry> registryList = mRegistryManager.getRegistriesFor(mUserId, dateString);

        // Look for the first user entrance and show the time in text view
        boolean enterFound = false;
        for (Registry registry : registryList) {
            Event event = mEventManager.getEvent(registry.getEventId());
            if (null != event && event.getType().equals(Event.EventType.EVENT_TYPE_ENTER)) {
                holder.mEnterValue.setText(registry.getTime());
                mEntranceTime[position] = registry.getTime();
                enterFound = true;
                break;
            }
        }

        // Look for the end of workday event and show the time in text view
        boolean exitFound = false;
        for (Registry registry : registryList) {
            Event event = mEventManager.getEvent(registry.getEventId());
            if (null != event && event.getId().equals(EventManager.END_EVENT_ID)) {
                holder.mExitValue.setText(registry.getTime());
                mExitTime[position] = registry.getTime();
                exitFound = true;
                break;
            }
        }

        // If no entrance was found, then apply null values
        if (!enterFound) {
            holder.mEnterValue.setText("--:--:--");
            mEntranceTime[position] = "--:--:--";
        }

        // If no end of workday was found, then apply null values
        if (!exitFound) {
            holder.mExitValue.setText("--:--:--");
            mExitTime[position] = "--:--:--";
        }

        // Calculates worktime for selected day, formats in and show
        WorkingHourUtils workingTimeUtils = new WorkingHourUtils(mRegistryManager, mEventManager);
        long worktime = workingTimeUtils.getTimeInRegistryList(registryList);
        mDaysWorktime[position] = worktime;

        int seconds = (int) (worktime / 1000) % 60;
        int minutes = (int) ((worktime / (1000 * 60)) % 60);
        int hours = (int) ((worktime / (1000 * 60 * 60)) % 24);
        String secondsString = seconds < 10 ? "0" + seconds : String.valueOf(seconds);
        String minutesString = minutes < 10 ? "0" + minutes : String.valueOf(minutes);
        String hoursString = minutes < 10 ? "0" + hours : String.valueOf(hours);
        holder.mHoursValue.setText(hoursString + ":" + minutesString + ":" + secondsString);
    }

    @Override
    public int getItemCount() {
        // Create a calendar object and set year and month
        int year = Integer.parseInt(mYear);
        int month = WorkingHourUtils.monthValueMap.get(mMonth) - 1;
        Calendar mycal = new GregorianCalendar(year, month, 1);

        // The items count will be the number of days in month
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return daysInMonth;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
