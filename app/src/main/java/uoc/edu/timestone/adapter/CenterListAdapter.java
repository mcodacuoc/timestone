package uoc.edu.timestone.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import uoc.edu.timestone.R;
import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.CompanyManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Company;
import uoc.edu.timestone.ui.system.CenterEditActivity;

/**
 * This class implements a RecyclerView adapter for center list activity
 */
public class CenterListAdapter extends RecyclerView.Adapter<CenterListAdapter.CenterListViewHolder>
        implements ValueEventListener {
    private static final String TAG = "CenterListAdapter";
    private CenterManager mCenterManager;
    private CompanyManager mCompanyManager;
    private Context mContext;

    /**
     * CenterListAdapter constructor
     *
     * @param context Parent activity context
     */
    public CenterListAdapter(Context context) {
        mCenterManager = new CenterManager(FirebaseDatabase.getInstance());
        mCenterManager.addValueEventListener(this);
        mCompanyManager = new CompanyManager(FirebaseDatabase.getInstance());
        mCompanyManager.addValueEventListener(this);
        this.mContext = context;
    }

    /**
     * This class provides a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    public static class CenterListViewHolder extends RecyclerView.ViewHolder {
        public Center mCenter;
        public View mView;
        TextView mNameTextView;
        TextView mAddressTextView;
        TextView mPostalCodeTextView;
        ImageButton mEditButton;

        /**
         * CenterListViewHolder class constructor
         *
         * @param itemView
         */
        public CenterListViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView​ = itemView;
            mNameTextView = itemView.findViewById(R.id.firstLine);
            mAddressTextView = itemView.findViewById(R.id.secondLine);
            mPostalCodeTextView = itemView.findViewById(R.id.thirdLine);
            mEditButton = itemView.findViewById(R.id.editCenterButton);
        }
    }

    @NonNull
    @Override
    public CenterListAdapter.CenterListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.center_list_item, parent, false);
        CenterListAdapter.CenterListViewHolder vh = new CenterListAdapter.CenterListViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CenterListAdapter.CenterListViewHolder holder, int position) {
        final Center center = mCenterManager.getCenters().get(position);
        holder.mNameTextView.setText(center.getName());
        holder.mAddressTextView.setText(center.getAddress());
        holder.mPostalCodeTextView.setText(center.getCp() + ", " + center.getCity());


        // Add action to edit center info on edit button clicked.
        // Selected center will be pass as a parameter to the edit activity
        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(mContext, CenterEditActivity.class);
                activityIntent.putExtra(CenterEditActivity.EDIT_MODE, true);
                activityIntent.putExtra(CenterEditActivity.SELECTED_CENTER, center);
                ((Activity) mContext).startActivityForResult(activityIntent,
                        CenterEditActivity.EDIT_CENTER_REQUEST);
            }
        });
    }

    /**
     * This method adds a new center to dataset
     *
     * @param center The new center created
     */
    public void addCenter(Center center) {
        Company company = mCompanyManager.getCompany(center.getCompanyId());
        mCenterManager.addCenter(center.getName(), center.getCity(), center.getAddress(), center.getCp(), company);
    }

    /**
     * This method updates center passed as parameter
     *
     * @param center Center containing modified information
     */
    public void updateCenter(Center center) {
        mCenterManager.updateCenter(center);
    }

    /**
     * Removes an center from data set
     *
     * @param center Center to be removed
     */
    public void removeCenter(Center center) {
        mCenterManager.removeCenter(center);
    }

    @Override
    public int getItemCount() {
        return mCenterManager.getCenters().size();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        this.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Error loading center list");
    }
}
