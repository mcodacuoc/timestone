package uoc.edu.timestone.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import uoc.edu.timestone.model.User;

/**
 * This class implements method to creates a PDF report
 */
public class ReportUtils {

    /**
     * This method creates a PDF report containing the workhours, entrances en exit
     * for a specified month of year and user.
     *
     * @param user         The report user
     * @param year         The report year
     * @param month        The report month
     * @param worktime     The month total working time
     * @param daysWorktime A list a working hours per day
     * @param entranceTime A list of entrance hours for each day
     * @param exitTime     A list of exit times for each day
     * @return The path of the generated report document
     * @throws IOException
     * @throws DocumentException
     */
    public static String generateReport(User user, String year, String month, long worktime,
                                        Long[] daysWorktime, String[] entranceTime,
                                        String[] exitTime) throws IOException, DocumentException {
        // Creates PDF document
        String path = android.os.Environment.getExternalStorageDirectory().toString();
        Document document = new Document();

        // Use user name, year and month for dcoument name
        PdfWriter.getInstance(document, new FileOutputStream(path + "/" + user.getName() +
                user.getSurname1() + user.getSurname2() + "_" + year + "_" + month + ".pdf"));

        document.open();

        // Set document's author, creation date and title.
        document.addAuthor("Timestone");
        document.addCreationDate();
        document.addTitle("Informe " + user.getName() + " " + user.getSurname1() +
                " " + user.getSurname2() + " " + year + " " + month);

        // Creates document's header
        document.add(createHeader(user, year, month, worktime));


        // The number of table rows will be the number of days in month
        int yearInt = Integer.parseInt(year);
        int monthInt = WorkingHourUtils.monthValueMap.get(month) - 1;
        Calendar mycal = new GregorianCalendar(yearInt, monthInt, 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

        // For each day, generates a new entry with work hours, enter time and exit time
        for (int i = 0; i < daysInMonth; i++) {
            document.add(createDayEntry(yearInt, monthInt, i + 1, daysWorktime[i],
                    entranceTime[i], exitTime[i]));
        }

        document.close();

        // Returns document path
        return path + "/" + user.getName() +
                user.getSurname1() + user.getSurname2() + "_" + year + "_" + month + ".pdf";
    }

    /**
     * This method creates document header
     *
     * @param user     The report user
     * @param year     The report year
     * @param month    The report month
     * @param worktime The month total worktime
     * @return The document header
     */
    private static PdfPTable createHeader(User user, String year, String month, long worktime) {

        int seconds = (int) (worktime / 1000) % 60;
        int minutes = (int) ((worktime / (1000 * 60)) % 60);
        int hours = (int) ((worktime / (1000 * 60 * 60)) % 24);
        String secondsString = seconds < 10 ? "0" + seconds : String.valueOf(seconds);
        String minutesString = minutes < 10 ? "0" + minutes : String.valueOf(minutes);

        // Creates a table with two columns with user data, month and total working hours
        PdfPTable table = new PdfPTable(2);
        table.addCell(user.getName() + " " + user.getSurname1() + " " + user.getSurname2());
        table.addCell("DNI:" + user.getId());
        table.addCell(month + " " + year);
        table.addCell("Horas: " + hours + ":" + minutesString + ":" + secondsString);
        table.setSpacingAfter(10);

        return table;
    }

    /**
     * This method creates an entry for a working day
     *
     * @param year      The year of the entry
     * @param month     The month of the entry
     * @param day       The day of the entry
     * @param workhours The day working hours
     * @param entrance  The entrance time
     * @param exit      The exit time
     * @return The day entry for document
     */
    private static PdfPTable createDayEntry(int year, int month, int day, Long workhours,
                                            String entrance, String exit) {
        // Creates a PDF table with two rows
        PdfPTable table = new PdfPTable(2);

        // Calculates date string
        Calendar mycal = new GregorianCalendar(year, month, day);
        String dateString = DateTimeUtils.toDateString(mycal.getTime());

        // Calculates the time string
        int seconds = (int) (workhours / 1000) % 60;
        int minutes = (int) ((workhours / (1000 * 60)) % 60);
        int hours = (int) ((workhours / (1000 * 60 * 60)) % 24);
        String secondsString = seconds < 10 ? "0" + seconds : String.valueOf(seconds);
        String minutesString = minutes < 10 ? "0" + minutes : String.valueOf(minutes);

        // Add date and time to entry
        table.addCell(dateString);
        table.addCell("Horas:" + hours + ":" + minutesString + ":" + secondsString);

        // Creates a row row the entrance time
        PdfPCell cell1 = new PdfPCell(new Phrase("Entrada:" + entrance));
        table.addCell(cell1);

        // Creates a row with the exit time
        PdfPCell cell2 = new PdfPCell(new Phrase("Salida:" + exit));
        table.addCell(cell2);

        table.setSpacingAfter(10);

        return table;
    }
}
