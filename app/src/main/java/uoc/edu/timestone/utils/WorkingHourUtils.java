package uoc.edu.timestone.utils;

import android.graphics.Color;
import android.util.Pair;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.service.NotificationUtils;

public class WorkingHourUtils {
    private RegistryManager mRegistryManager;
    private EventManager mEventManager;

    public static final Map<String, Integer> monthValueMap;

    static {
        monthValueMap = new HashMap<>();
        monthValueMap.put("Enero", 1);
        monthValueMap.put("Febrero", 2);
        monthValueMap.put("Marzo", 3);
        monthValueMap.put("Abril", 4);
        monthValueMap.put("Mayo", 5);
        monthValueMap.put("Junio", 6);
        monthValueMap.put("Julio", 7);
        monthValueMap.put("Agosto", 8);
        monthValueMap.put("Septiembre", 9);
        monthValueMap.put("Octubre", 10);
        monthValueMap.put("Noviembre", 11);
        monthValueMap.put("Diciembre", 12);
    }

    public WorkingHourUtils(RegistryManager registryManager, EventManager eventManager) {
        this.mRegistryManager = registryManager;
        this.mEventManager = eventManager;
    }

    /**
     * This method returns the specified user worked time for a year and month in milliseconds
     *
     * @param yearString  The year of the report
     * @param monthString The month of the report
     * @param userId      The user identifier to obtain report
     * @return The time in milliseconds worked the specified month
     */
    public long calculateWorkTime(String yearString, String monthString, String userId) {
        int year = Integer.parseInt(yearString);
        int month = monthValueMap.get(monthString);

        // Create a calendar object and set year and month
        Calendar mycal = new GregorianCalendar(year, month - 1, 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

        int worktime = 0;
        for (int i = 1; i < daysInMonth + 1; i++) {
            Calendar cal = Calendar.getInstance();
            cal.set(year, month - 1, i, 0, 0, 0);
            String dateString = DateTimeUtils.toDateString(cal.getTime());

            List<Registry> registryList = mRegistryManager.getRegistriesFor(userId, dateString);

            worktime += getTimeInRegistryList(registryList);
        }

        return worktime;
    }

    /**
     * This method returns the time worked in milliseconds specified by a list of registries
     *
     * @param list The list of registries
     * @return The worked time in milliseconds worked
     */
    public long getTimeInRegistryList(List<Registry> list) {
        long result = 0;

        // Obtain the first date of the list
        Date lastDate = Calendar.getInstance().getTime();
        if (!list.isEmpty()) {
            try {
                lastDate = DateTimeUtils.toTimestamp(list.get(0).getDate(), list.get(0).getTime());
            } catch (ParseException e) {
                return result;
            }
        }

        // Then, compare each new event date with the last one and calculates each subtime
        for (int i = 1; i < list.size(); i++) {
            try {
                Event event = mEventManager.getEvent(list.get(i - 1).getEventId());
                Date newDate = DateTimeUtils.toTimestamp(list.get(i).getDate(), list.get(i).getTime());

                // If the registry owns to a non working event, then this time will be omitted
                if (null != event && event.isWorking()) {
                    result += Math.abs(newDate.getTime() - lastDate.getTime());
                }
                lastDate = newDate;

            } catch (ParseException e) {
                return result;
            }
        }

        // Finally, return the total amount of time
        return result;
    }

    /**
     * This method updates the event time container based on a registry list
     *
     * @param list The list of registries used to update
     */
    public void updateChartEntryData(List<Registry> list, EntryData entryData) {
        long total = 0;

        // Obtain the first date of the list
        Date lastDate = Calendar.getInstance().getTime();
        if (!list.isEmpty()) {
            try {
                lastDate = DateTimeUtils.toTimestamp(list.get(0).getDate(), list.get(0).getTime());
            } catch (ParseException e) {
                return;
            }
        }

        // Then, compare each new event date with the last one and calculates each subtime
        for (int i = 1; i < list.size(); i++) {
            try {
                Event event = mEventManager.getEvent(list.get(i - 1).getEventId());
                Date newDate = DateTimeUtils.toTimestamp(list.get(i).getDate(), list.get(i).getTime());

                // If the registry not owns to the same day, then this time will be omitted
                if (list.get(i).getDate().equals(list.get(i - 1).getDate())) {
                    // Add elapsed time to its corresponding event and set color
                    int color = Color.parseColor(event.getColor());
                    Long time = Math.abs(newDate.getTime() - lastDate.getTime());

                    if (null == entryData.getEventMap().get(event.getId())) {
                        Pair<Long, Integer> value = new Pair<>(time, color);
                        entryData.getEventMap().put(event.getId(), value);
                    } else {
                        Long initialTime = entryData.getEventMap().get(event.getId()).first;
                        Pair<Long, Integer> value = new Pair<>(initialTime + time, color);
                        entryData.getEventMap().put(event.getId(), value);
                    }

                    // Increments the total registered hours
                    total += Math.abs(newDate.getTime() - lastDate.getTime());
                }

                lastDate = newDate;

            } catch (ParseException e) {
                return;
            }
        }

        // Increments the total time
        entryData.setTotalTime(entryData.getTotalTime() + total);
    }

    public void checkForErrors(List<Registry> list, User user, String date) {
        boolean initWorkFound = false;
        boolean endWorkFound = false;
        String centerId = "";

        for (Registry registry : list) {
            Event event = mEventManager.getEvent(registry.getEventId());

            if (!initWorkFound) {
                if (null != event && event.getId().equals(EventManager.ENTER_EVENT_ID)) {
                    initWorkFound = true;
                    centerId = registry.getCenterId();
                }
            } else {
                if (null != event && event.getId().equals(EventManager.END_EVENT_ID)) {
                    endWorkFound = true;
                    break;
                }
            }
        }

        if (initWorkFound && !endWorkFound) {
            NotificationUtils.notifyExitError(user, centerId, date);
        }
    }
}
