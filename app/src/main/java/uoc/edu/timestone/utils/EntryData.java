package uoc.edu.timestone.utils;

import android.util.Pair;

import java.util.HashMap;
import java.util.Observable;

/**
 * This class contains the hours dedicated to each kind of event
 */
public class EntryData extends Observable {
    private HashMap<String, Pair<Long, Integer>> mEventMap = new HashMap<>();
    private long mTotalTime;

    public HashMap<String, Pair<Long, Integer>> getEventMap() {
        return mEventMap;
    }

    public long getTotalTime() {
        return mTotalTime;
    }

    public void setTotalTime(long totalTime) {
        this.mTotalTime = totalTime;
    }

    public void setUpdated() {
        setChanged();
        notifyObservers();
    }

    public void clear() {
        mEventMap.clear();
        setChanged();
        notifyObservers();
    }
}
