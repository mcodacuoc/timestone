package uoc.edu.timestone.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class implements methods for date and time format
 */
public class DateTimeUtils {
    private final static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private final static DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private final static DateFormat timestampFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss'Z'");

    /**
     * This method obtains a string with date passed as parameter
     *
     * @param date The date to be formatted
     * @return A date string with dd/MM/yyyy format
     */
    public static String toDateString(Date date) {
        return dateFormat.format(date);
    }

    /**
     * This method obtains a string with time passed as parameter
     *
     * @param date The time passed as parameter
     * @return A time string with HH:mm:ss format
     */
    public static String toTimeString(Date date) {
        return timeFormat.format(date);
    }

    public static Date toTimestamp(String dateString, String timeString) throws ParseException {
        return timestampFormat.parse(dateString + "T" + timeString + "Z");
    }
}
