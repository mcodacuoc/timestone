package uoc.edu.timestone.service;

import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;

import uoc.edu.timestone.manager.AlertManager;
import uoc.edu.timestone.model.Alert;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.utils.DateTimeUtils;

/**
 * This class handles user enter and exit events. Also manages errors in work hour imputations
 */
public class NotificationUtils {
    private static AlertManager mAlertManager = new AlertManager(FirebaseDatabase.getInstance());

    /**
     * This method registers a new notification for an user enter.
     *
     * @param user   User have been detected
     * @param device Device that detects the entrance
     */
    static public void notifyUserEnter(User user, Device device) {
        Date date = Calendar.getInstance().getTime();
        String dateString = DateTimeUtils.toDateString(date);
        String timeString = DateTimeUtils.toTimeString(date);
        mAlertManager.addAlert(user.getId(),
                device.getCenterId(), Alert.AlertType.ALERT_TYPE_ENTER,
                dateString, timeString);
    }

    /**
     * This method registers a new notification for an user enter
     *
     * @param user   User have been detected
     * @param device Device that detects the entrance
     * @param time   Moment of the entrance event
     */
    static public void notifyUserEnter(User user, Device device, Calendar time) {
        Date date = time.getTime();
        String dateString = DateTimeUtils.toDateString(date);
        String timeString = DateTimeUtils.toTimeString(date);
        mAlertManager.addAlert(user.getId(),
                device.getCenterId(), Alert.AlertType.ALERT_TYPE_ENTER,
                dateString, timeString);
    }


    /**
     * This method registers a new notification for an user exit.
     *
     * @param user   User have been detected
     * @param device Device that detects the exit
     */
    static public void notifyUserExit(User user, Device device) {
        Date date = Calendar.getInstance().getTime();
        String dateString = DateTimeUtils.toDateString(date);
        String timeString = DateTimeUtils.toTimeString(date);
        mAlertManager.addAlert(user.getId(),
                device.getCenterId(), Alert.AlertType.ALERT_TYPE_EXIT,
                dateString, timeString);
    }

    /**
     * This method registers a new notification for an workday error
     *
     * @param user     User to be notified
     * @param centerId Center related to notification
     * @param date     Date of the error
     */
    static public void notifyExitError(User user, String centerId, String date) {
        mAlertManager.addAlert(user.getId(),
                centerId, Alert.AlertType.ALERT_TYPE_ERROR,
                date, "23:59:59");
    }
}
