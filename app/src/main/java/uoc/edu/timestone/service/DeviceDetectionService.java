package uoc.edu.timestone.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.annotation.Nullable;
import uoc.edu.timestone.manager.DeviceManager;
import uoc.edu.timestone.model.Device;

/**
 * This class implements a detection service for non registered beacons.
 */
public class DeviceDetectionService extends Service implements BeaconConsumer {
    protected static final String TAG = "DeviceDetectionService";
    private BeaconManager mBeaconManager;
    private DeviceManager mDeviceManager;
    private static DeviceDetectionService.DeviceDetectionContainer mDeviceContainer =
            new DeviceDetectionService.DeviceDetectionContainer();

    /**
     * This inner class implements a temporary storage for non registered beacons
     * have been detected.
     */
    static class DeviceDetectionContainer extends Observable {
        private List<Device> mDetectedDevices = new ArrayList<>();

        /**
         * This method adds a new detected device
         *
         * @param uuid  The device identifier
         * @param major The device major number
         * @param minor The device minor number
         */
        public void addDetectedDevice(String uuid, int major, int minor) {
            Device device = new Device(uuid, major, minor, Device.DeviceType.DEVICE_TYPE_ENTRANCE);
            mDetectedDevices.add(device);
            setChanged();
            notifyObservers();
        }

        /**
         * This method updates the specified device in detected list
         *
         * @param uuid The device identifier to be updated
         */
        public void updateDetectedDevice(String uuid, double distance) {
            for (int i = 0; i < mDetectedDevices.size(); i++) {
                if (mDetectedDevices.get(i).getId().equals(uuid)) {
                    mDetectedDevices.get(i).setDistance(distance);
                    setChanged();
                    break;
                }
            }
            notifyObservers();
        }

        /**
         * This method removes the specified device from detected list
         *
         * @param uuid The device uuid to remove
         */
        public void removeDetectedDevice(String uuid) {
            for (int i = 0; i < mDetectedDevices.size(); i++) {
                if (mDetectedDevices.get(i).getId().equals(uuid)) {
                    mDetectedDevices.remove(i);
                    setChanged();
                    break;
                }
            }
            notifyObservers();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Initialize device database manager
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDeviceManager = new DeviceManager(database);

        // Initialize beacon detection service
        mBeaconManager = BeaconManager.getInstanceForApplication(this);
        mBeaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        mBeaconManager.bind(this);

        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * This method adds an observer object for detected beacons storage changes
     *
     * @param o Observer object
     */
    public static void addObserver(Observer o) {
        mDeviceContainer.addObserver(o);
    }

    /**
     * This method returns the list of new detected devices
     *
     * @return The current list of detected devices
     */
    public static List<Device> getDetectedDevices() {
        return mDeviceContainer.mDetectedDevices;
    }

    /**
     * This method returns the detected device with the specified uuid
     *
     * @param uuid The device identifier to be found
     * @return The device found, returns null if no exists
     */
    public static Device getDetectedDevice(String uuid) {
        Device result = null;
        if (null == result) {
            for (Device device : mDeviceContainer.mDetectedDevices) {
                if (device.getId().equals(uuid)) {
                    result = device;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * This method start beacon region monitoring and ranging detection
     */
    public void startBeaconDetection() {
        // Starts detection for non registered beacons
        try {
            Region region = new Region("uoc.edu.timestone.backgroundRegion",
                    null, null, null);
            mBeaconManager.startMonitoringBeaconsInRegion(region);
            mBeaconManager.startRangingBeaconsInRegion(region);
        } catch (
                RemoteException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    /**
     * This method processes a region enter event
     *
     * @param region The region entered
     */
    public void processEnterRegion(Region region) {
        if (null != region.getId1()) {
            Device device = mDeviceManager.getDevice(region.getId1().toString());

            // If device is not registered in database will be processed
            if (null == device) {
                device = getDetectedDevice(region.getId1().toString());

                // If device is not in detection list, then will be added to detection list
                if (null == device) {
                    mDeviceContainer.addDetectedDevice(region.getId1().toString(),
                            region.getId2().toInt(), region.getId3().toInt());
                }
            }
        }
    }

    /**
     * This method processes a region exit event
     *
     * @param region The region exited
     */
    public void processExitRegion(Region region) {
        if (null != region.getId1()) {
            Device device = mDeviceManager.getDevice(region.getId1().toString());

            // If device is not registered in database will be processed
            if (null == device) {
                device = getDetectedDevice(region.getId1().toString());

                // If detected device was previously detected, then will be removed from list
                if (null != device) {
                    Log.d(TAG, "processExitRegion for: " + device.getId());
                    mDeviceContainer.removeDetectedDevice(device.getId());
                }
            }
        }
    }

    /**
     * This method processes a beacon raging event
     *
     * @param beacon Beacon detected
     */
    public void processBeaconRange(Beacon beacon) {
        if (null != beacon.getId1()) {
            Device device = mDeviceManager.getDevice(beacon.getId1().toString());

            // If device is not registered in database will be processed
            if (null == device) {
                device = getDetectedDevice(beacon.getId1().toString());

                // If device is already in detection list, then updates it
                if (null != device) {
                    mDeviceContainer.updateDetectedDevice(
                            device.getId(), beacon.getDistance());
                } else { // Otherwise, beacon will be added
                    mDeviceContainer.addDetectedDevice(beacon.getId1().toString(),
                            beacon.getId2().toInt(), beacon.getId3().toInt());
                }
            }
            // If device is already registered, remove from detected list
            else {
                if (null != getDetectedDevice(device.getId())) {
                    mDeviceContainer.removeDetectedDevice(device.getId());
                }
            }
        }
    }

    /**
     * This method is called when beacon service connects
     */
    @Override
    public void onBeaconServiceConnect() {
        mBeaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Log.d(TAG, "Got a didEnterRegion call");
                processEnterRegion(region);
            }

            @Override
            public void didExitRegion(Region region) {
                Log.d(TAG, "Got a didDetermineStateRegion call");
                processEnterRegion(region);
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                Log.d(TAG, "Got a didExitRegion call");
                processExitRegion(region);
            }
        });
        mBeaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
                Log.d(TAG, "didRangeBeaconsInRegion with size " + collection.size());
                if (collection.size() > 0) {
                    for (Beacon beacon : collection) {
                        processBeaconRange(beacon);
                    }
                }
            }
        });

        startBeaconDetection();
    }
}
