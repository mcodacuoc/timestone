package uoc.edu.timestone.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import uoc.edu.timestone.MainActivity;
import uoc.edu.timestone.MainEmptyActivity;
import uoc.edu.timestone.R;
import uoc.edu.timestone.TimestoneApplication;
import uoc.edu.timestone.manager.DeviceManager;

import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.utils.DateTimeUtils;
import uoc.edu.timestone.utils.WorkingHourUtils;

/**
 * This class implements a beacon detection foreground service.
 * This service only detects previously registered beacons
 */
public class DeviceMonitoringService extends Service implements BootstrapNotifier, ValueEventListener {
    protected static final String TAG = "DeviceMonitoringService";
    private static final int ENTER_NOTIFICATION_ID = 1;
    private static final int EXIT_NOTIFICATION_ID = 2;

    /**
     * This class stores temporary notifications
     */
    private class DetectionMark {
        private Calendar mTime;
        private boolean mExternal;
        private List<String> mInternalDevices = new ArrayList<>();

        public DetectionMark(Calendar time) {
            this.mTime = time;
        }

        public Calendar getTime() {
            return mTime;
        }

        public void setTime(Calendar time) {
            this.mTime = time;
        }

        private boolean haveInternalMark() {
            return !mInternalDevices.isEmpty();
        }

        private void removeInternalMark(String deviceId) {
            mInternalDevices.remove(deviceId);
        }

        private void addInternalMark(String deviceId) {
            if (mInternalDevices.indexOf(deviceId) < 0) {
                mInternalDevices.add(deviceId);
            }
        }

        private boolean haveExternalMark() {
            return mExternal;
        }

        private void setExternalMark(boolean external) {
            this.mExternal = external;
        }
    }

    private BeaconManager mBeaconManager;
    private DeviceManager mDeviceManager;
    private RegistryManager mRegistryManager;
    private EventManager mEventManager;
    private NotificationChannel mNotificationChannel;
    private BackgroundPowerSaver mBackgroundPowerSaver;
    private RegionBootstrap mRegionBootstrap;
    private List<Region> mRegionList = new ArrayList<>();
    private DetectionMark mDetectionMark = null;
    private WorkingHourUtils mWorkingHourUtils;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Initialize device database manager
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDeviceManager = new DeviceManager(database);
        mRegistryManager = new RegistryManager(database);
        mEventManager = new EventManager(database);

        mDetectionMark = new DetectionMark(Calendar.getInstance());
        mWorkingHourUtils = new WorkingHourUtils(mRegistryManager, mEventManager);

        // Initialize beacon monitoring service
        mBeaconManager = BeaconManager.getInstanceForApplication(this);
        mBeaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        // Initialize the notification channels for foreground service
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle(getString(R.string.app_notificaction_title));
        Intent toGoIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, toGoIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationChannel = new NotificationChannel(TimestoneApplication.NOTIFICATION_ID,
                    "Foreground Notification", NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationChannel.setDescription("Timestone Foreground Notification Channel");
            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mNotificationChannel);
            builder.setChannelId(mNotificationChannel.getId());
        }

        // This line enables the foreground service scanning
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mBeaconManager.enableForegroundServiceScanning(builder.build(), 456);
        }

        // For the above foreground scanning service to be useful, you need to disable
        // JobScheduler-based scans (used on Android 8+) and set a fast background scan
        // cycle that would otherwise be disallowed by the operating system.
        mBeaconManager.setEnableScheduledScanJobs(false);
        mBeaconManager.setBackgroundBetweenScanPeriod(0);
        mBeaconManager.setBackgroundScanPeriod(1100);

        try {
            mBeaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            Log.e(TAG, e.getCause().getMessage());
        }

        // simply constructing this class and holding a reference to it in your custom Application
        // class will automatically cause the BeaconLibrary to save battery whenever the application
        // is not visible.  This reduces bluetooth power usage by about 60%
        mBackgroundPowerSaver = new BackgroundPowerSaver(this);

        mDeviceManager.addValueEventListener(this);

        // Create the Handler object (on the main thread by default)
        final Handler handler = new Handler();
        // Define the code block to be executed
        Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                // Do something here on the main thread
                checkForErrors();

                // Check errors each 12 hours
                handler.postDelayed(this, 1000 * 60 * 60 * 12);
            }
        };

        handler.post(runnableCode);

        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * This method process the enter in region events
     *
     * @param region The region entered
     */
    public void processEnterRegion(Region region) {
        if (null != region.getId1()) {
            Device device = mDeviceManager.getDevice(region.getId1().toString());

            // If device is already registered
            if (null != device) {
                Log.d(TAG, "processEnterRegion for: " + device.getId());

                // For entrance devices detection
                if (device.getType().equals(Device.DeviceType.DEVICE_TYPE_ENTRANCE)) {

                    // Mark user is in external region
                    mDetectionMark.setTime(Calendar.getInstance());
                    mDetectionMark.setExternalMark(true);

                    // Look for interior devices for the same center
                    List<Device> interiorDevices = mDeviceManager.
                            getDevicesFor(device.getCenterId(), Device.DeviceType.DEVICE_TYPE_INTERIOR);

                    // If there is only devices in the entrance, then notify the event
                    if (interiorDevices.isEmpty()) {
                        // Stores a notification in database
                        NotificationUtils.notifyUserEnter(MainEmptyActivity.mCurrentUser, device);

                        // Creates a notification to alert user
                        sendDeviceNotfication(ENTER_NOTIFICATION_ID,
                                getString(R.string.notification_enter_description),
                                Calendar.getInstance());
                    }

                } else {
                    // For internal devices detection
                    Calendar notificationTime = Calendar.getInstance();

                    // If there is previous stored temporary notification, use this
                    // time information to notify
                    if (mDetectionMark.haveExternalMark()) {
                        notificationTime = mDetectionMark.getTime();
                        mDetectionMark.setExternalMark(false);
                    }

                    if (!mDetectionMark.haveInternalMark()) {
                        // Stores a notification in database
                        NotificationUtils.notifyUserEnter(MainEmptyActivity.mCurrentUser,
                                device, notificationTime);

                        // Creates a notification to alert user
                        sendDeviceNotfication(ENTER_NOTIFICATION_ID,
                                getString(R.string.notification_enter_description),
                                notificationTime);
                    }

                    // Mark user is in internal region
                    mDetectionMark.addInternalMark(device.getId());
                }
            }
        }
    }

    /**
     * This method process the exit region events
     *
     * @param region The region exited
     */
    public void processExitRegion(Region region) {
        if (null != region.getId1()) {
            Device device = mDeviceManager.getDevice(region.getId1().toString());

            // If device is not registered in database, its a detected one
            if (null != device) {
                Log.d(TAG, "processExitRegion for: " + device.getId());

                if (device.getType().equals(Device.DeviceType.DEVICE_TYPE_ENTRANCE)) {
                    // If user is not in internal region, then notify exit
                    if (!mDetectionMark.haveInternalMark()) {
                        // Stores a notification in database
                        NotificationUtils.notifyUserExit(MainEmptyActivity.mCurrentUser, device);

                        // Creates a notification to alert user
                        sendDeviceNotfication(EXIT_NOTIFICATION_ID,
                                getString(R.string.notification_exit_description), Calendar.getInstance());

                    }
                } else {
                    mDetectionMark.removeInternalMark(device.getId());
                }
            }
        }
    }

    /**
     * This method shows a device notification for device detection event
     *
     * @param id   The notification id
     * @param text The text to show on notification
     * @param cal  Date to show on notification message
     */
    public void sendDeviceNotfication(int id, String text, Calendar cal) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle(text + " " + DateTimeUtils.toTimeString(cal.getTime()));
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(mNotificationChannel.getId());
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(id, builder.build());
    }

    /**
     * This method disable all regions monitoring
     */
    public void disableForegroundMonitoring() {
        if (mRegionBootstrap != null) {
            mRegionBootstrap.disable();
            mRegionBootstrap = null;
        }
    }

    /**
     * This method enable a region monitoring for all registered devices
     */
    public void enableForegroundMonitoring() {
        // wake up the app when any beacon is seen (you can specify specific id filers in the
        // parameters below)
        int i = 1;
        for (Device device : mDeviceManager.getDevices()) {
            mRegionList.add(new Region("uoc.edu.timestone.backgroundRegion_device" + i,
                    Identifier.parse(device.getId()),
                    Identifier.parse(String.valueOf(device.getMajor())),
                    Identifier.parse(String.valueOf(device.getMinor()))));
            i++;
        }

        mRegionBootstrap = new RegionBootstrap(this, mRegionList);
    }

    /**
     * This method checks for error in the last day workday registers
     */
    private void checkForErrors() {
        if (null != MainEmptyActivity.mCurrentUser) {
            Calendar yesterday = Calendar.getInstance();
            yesterday.add(Calendar.DATE, -1);
            String yesterdayString = DateTimeUtils.toDateString(yesterday.getTime());
            List<Registry> list = mRegistryManager.getRegistriesFor(
                    MainEmptyActivity.mCurrentUser.getId(), yesterdayString);

            mWorkingHourUtils.checkForErrors(list, MainEmptyActivity.mCurrentUser, yesterdayString);
        }
    }

    @Override
    public void didEnterRegion(org.altbeacon.beacon.Region region) {
        Log.d(TAG, "Got a didEnterRegion call for " + region.getId1());
        processEnterRegion(region);
    }

    @Override
    public void didDetermineStateForRegion(int i, org.altbeacon.beacon.Region region) {
        Log.d(TAG, "Got a didDetermineStateRegion call for " + region.getId1());
    }

    @Override
    public void didExitRegion(org.altbeacon.beacon.Region region) {
        Log.d(TAG, "Got a didExitRegion call for " + region.getId1());
        processExitRegion(region);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        Log.d(TAG, "Device data changed");
        disableForegroundMonitoring();
        enableForegroundMonitoring();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Device data loading cancelled");
    }
}