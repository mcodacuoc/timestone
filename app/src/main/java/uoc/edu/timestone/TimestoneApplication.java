package uoc.edu.timestone;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import uoc.edu.timestone.service.DeviceMonitoringService;

/**
 * This class implements the application that will be running in foreground
 */
public class TimestoneApplication extends Application {
    private static final String TAG = "TimestoneApplication";
    public static final String NOTIFICATION_ID = "Timestone Notification Channel";


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "App started up");

        Intent intent = new Intent(this, DeviceMonitoringService.class);
        startService(intent);
    }
}
