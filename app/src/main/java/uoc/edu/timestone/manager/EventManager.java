package uoc.edu.timestone.manager;

import android.os.Build;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.Event;

/**
 * This class handles the creation, update, removes and query for event.
 */
public class EventManager implements ChildEventListener {
    public static final String ENTER_EVENT_ID = "0";
    public static final String END_EVENT_ID = "1";
    private static final Map<Event.EventType, String> eventTypeMap;

    static {
        eventTypeMap = new HashMap<>();
        eventTypeMap.put(Event.EventType.EVENT_TYPE_ENTER, "Entrada");
        eventTypeMap.put(Event.EventType.EVENT_TYPE_EXIT, "Salida");
    }

    private DatabaseReference mRef;
    private List<Event> mEvents = new ArrayList<>();
    static final String kDatabasereference = "events";

    /**
     * EventManager class constructor
     *
     * @param database The firebase database reference
     */
    public EventManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public EventManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    /**
     * This method adds a new event to database
     *
     * @param name    The name for the event
     * @param working <code>true</code> if the hours spent in this event will be considered to
     *                working hours calculation
     * @param type    The event type
     */
    public void addEvent(String name, boolean working, Event.EventType type, String color) {
        String key = mRef.push().getKey();
        if (null != key) {
            Event event = new Event(key, name, working, type, color);
            mRef.child(key).setValue(event);
        }
    }

    /**
     * This method updates the specified event in database
     *
     * @param event The event to be updated
     */
    public void updateEvent(Event event) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(event.getId(), event);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes the specified event from database
     *
     * @param event The event to be removed
     */
    public void removeEvent(Event event) {
        mRef.child(event.getId()).removeValue();
    }

    /**
     * This method returns the list of events.
     *
     * @return The current list of events
     */
    public List<Event> getEvents() {
        return mEvents;
    }

    /**
     * This method returns an event specified by id.
     *
     * @param id The identifier to be found
     * @return The event specified by id. Returns null if no exists
     */
    public Event getEvent(String id) {
        Event result = null;

        for (Event event : mEvents) {
            if (event.getId().equals(id)) {
                result = event;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns the list of events for specified type
     *
     * @param eventType The event type to filter by
     * @return The list of registries filtered by type
     */
    public List<Event> getEventForType(final Event.EventType eventType) {
        List<Event> result = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = mEvents.stream()
                    .filter(new Predicate<Event>() {
                        @Override
                        public boolean test(Event e) {
                            return e.getType().equals(eventType);
                        }
                    })
                    .collect(Collectors.<Event>toList());
        }
        return result;
    }

    /**
     * This method returns an event specified by name.
     *
     * @param name The identifier to be found
     * @return The event specified by name. Returns null if no exists.
     */
    public Event getEventByName(String name) {
        Event result = null;

        for (Event event : mEvents) {
            if (event.getName().equals(name)) {
                result = event;
                break;
            }
        }
        return result;
    }

    /**
     * This method returns the event type string from enum value
     *
     * @param type The event type enum value
     * @return Returns the evet type string
     */
    public String getEventTypeString(Event.EventType type) {
        return eventTypeMap.get(type);
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mEvents.add(dataSnapshot.getValue(Event.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        Event event = dataSnapshot.getValue(Event.class);
        if (null != event) {
            for (int i = 0; i < mEvents.size(); i++) {
                if (mEvents.get(i).getId().equals(event.getId())) {
                    mEvents.set(i, event);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Event event = dataSnapshot.getValue(Event.class);
        if (null != event) {
            for (int i = 0; i < mEvents.size(); i++) {
                if (mEvents.get(i).getId().equals(event.getId())) {
                    mEvents.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
