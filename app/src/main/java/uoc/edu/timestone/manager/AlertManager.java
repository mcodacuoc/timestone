package uoc.edu.timestone.manager;

import android.os.Build;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.Alert;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.utils.DateTimeUtils;

/**
 * This class handles the creation, update, removes and query for alerts.
 */
public class AlertManager implements ChildEventListener {
    private DatabaseReference mRef;
    private List<Alert> mAlerts = new ArrayList<>();
    static final String kDatabasereference = "alerts";

    /**
     * AlertManager class constructor
     *
     * @param database The firebase database reference
     */
    public AlertManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public AlertManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    /**
     * This method adds a new alert element into database
     *
     * @param user   The user associated to the alert
     * @param center The center in which the alert have been generated
     * @param type   The type of alert
     * @param date   The date for the alert
     * @param time   The time for teh alert
     */
    public void addAlert(User user, Center center, Alert.AlertType type, String date, String time) {
        String key = mRef.push().getKey();
        if (null != key) {
            Alert alert = new Alert(key, user, center, type, date, time);
            mRef.child(key).setValue(alert);
        }
    }

    /**
     * This method adds a new alert element into database
     *
     * @param user   The user associated to the alert
     * @param center The center in which the alert have been generated
     * @param type   The type of alert
     * @param date   The date for the alert
     * @param time   The time for teh alert
     */
    public void addAlert(String user, String center, Alert.AlertType type, String date, String time) {
        String key = mRef.push().getKey();
        if (null != key) {
            Alert alert = new Alert(key, user, center, type, date, time);
            mRef.child(key).setValue(alert);
        }
    }


    /**
     * This method updates an alert values in database
     *
     * @param alert The alert element to be modified
     */
    public void updateAlert(Alert alert) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(alert.getId(), alert);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes an alert from database
     *
     * @param alert The alert element to be removed
     */
    public void removeAlert(Alert alert) {
        mRef.child(alert.getId()).removeValue();
    }

    /**
     * This method returns the current list of registered alerts
     *
     * @return The current list of alerts
     */
    public List<Alert> getAlerts() {
        return mAlerts;
    }

    /**
     * This method returns an alert with the specified id.
     *
     * @param id The alert identifier for alert
     * @return The alert with the specified id. Returns null if no exists.
     */
    public Alert getAlert(String id) {
        Alert result = null;

        for (Alert alert : mAlerts) {
            if (alert.getId().equals(id)) {
                result = alert;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns the alerts related to specified user
     *
     * @param userId The specified user identifier
     * @return A list of alerts for the specified user
     */
    public List<Alert> getAlertsForUser(final String userId) {
        List<Alert> result = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = mAlerts.stream()
                    .filter(new Predicate<Alert>() {
                        @Override
                        public boolean test(Alert r) {
                            return r.getUserId().equals(userId);
                        }
                    }).sorted(new Comparator<Alert>() {
                        @Override
                        public int compare(Alert o1, Alert o2) {
                            int result = 1;
                            Date o1Timestamp;
                            Date o2Timestamp;
                            try {
                                o1Timestamp = DateTimeUtils.toTimestamp(o1.getDate(), o1.getTime());
                                o2Timestamp = DateTimeUtils.toTimestamp(o2.getDate(), o2.getTime());
                            } catch (ParseException e) {
                                return result;
                            }

                            if (o1Timestamp.after(o2Timestamp)) {
                                result = 1;
                            } else if (o1Timestamp.before(o2Timestamp)) {
                                result = -1;
                            } else {
                                result = 0;
                            }
                            return result;
                        }
                    }).collect(Collectors.<Alert>toList());
        }
        return result;
    }

    /**
     * This method returns a list of alerts for specified type
     *
     * @param alertType The type of alerts to be returned
     * @return A list of alerts of specified type
     */
    public List<Alert> getAlertsForType(final Alert.AlertType alertType) {
        List<Alert> result = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = mAlerts.stream()
                    .filter(new Predicate<Alert>() {
                        @Override
                        public boolean test(Alert r) {
                            return r.getType().equals(alertType);
                        }
                    }).sorted(new Comparator<Alert>() {
                        @Override
                        public int compare(Alert o1, Alert o2) {
                            int result = 1;
                            Date o1Timestamp;
                            Date o2Timestamp;
                            try {
                                o1Timestamp = DateTimeUtils.toTimestamp(o1.getDate(), o1.getTime());
                                o2Timestamp = DateTimeUtils.toTimestamp(o2.getDate(), o2.getTime());
                            } catch (ParseException e) {
                                return result;
                            }
                            if (o1Timestamp.after(o2Timestamp)) {
                                result = 1;
                            } else if (o1Timestamp.before(o2Timestamp)) {
                                return -1;
                            } else {
                                return 0;
                            }
                            return result;
                        }
                    }).collect(Collectors.<Alert>toList());
        }
        return result;
    }


    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mAlerts.add(dataSnapshot.getValue(Alert.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        Alert alert = dataSnapshot.getValue(Alert.class);
        if (null != alert) {
            for (int i = 0; i < mAlerts.size(); i++) {
                if (mAlerts.get(i).getId().equals(alert.getId())) {
                    mAlerts.set(i, alert);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Alert alert = dataSnapshot.getValue(Alert.class);
        if (null != alert) {
            for (int i = 0; i < mAlerts.size(); i++) {
                if (mAlerts.get(i).getId().equals(alert.getId())) {
                    mAlerts.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
