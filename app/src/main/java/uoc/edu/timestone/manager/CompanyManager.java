package uoc.edu.timestone.manager;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.Company;

/**
 * This class handles the creation, update, removes and query for companies.
 */
public class CompanyManager implements ChildEventListener {
    private DatabaseReference mRef;
    private List<Company> mCompanies = new ArrayList<>();
    static final String kDatabasereference = "company";

    /**
     * CompanyManager class constructor
     *
     * @param database The firebase database reference
     */
    public CompanyManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public CompanyManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    /**
     * This method adds a new company to database
     *
     * @param cif  The legal identifier for company
     * @param name The name of the company
     */
    public void addCompany(String cif, String name) {
        Company company = new Company(cif, name);
        mRef.child(cif).setValue(company);
    }

    /**
     * This method updates the specified company in database
     *
     * @param company The company to be modified
     */
    public void updateCompany(Company company) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(company.getId(), company);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes the specified company from database
     *
     * @param company The company to be removed
     */
    public void removeCompany(Company company) {
        mRef.child(company.getId()).removeValue();
    }

    /**
     * This method returns the list of registered companies
     *
     * @return The current list of companies
     */
    public List<Company> getCompanies() {
        return mCompanies;
    }

    /**
     * This method returns the company specified by its cif
     *
     * @param cif The identifier of the company to be found
     * @return The company specified by id. Returns null if no exists
     */
    public Company getCompany(String cif) {
        Company result = null;

        for (Company company : mCompanies) {
            if (company.getId().equals(cif)) {
                result = company;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns the company specified by its name
     *
     * @param name The name of the company to be found
     * @return The company specified by name. Returns null if no exists
     */
    public Company getCompanyByName(String name) {
        Company result = null;

        for (Company company : mCompanies) {
            if (company.getName().equals(name)) {
                result = company;
                break;
            }
        }

        return result;
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mCompanies.add(dataSnapshot.getValue(Company.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        Company company = dataSnapshot.getValue(Company.class);
        if (null != company) {
            for (int i = 0; i < mCompanies.size(); i++) {
                if (mCompanies.get(i).getId().equals(company.getId())) {
                    mCompanies.set(i, company);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Company company = dataSnapshot.getValue(Company.class);
        if (null != company) {
            for (int i = 0; i < mCompanies.size(); i++) {
                if (mCompanies.get(i).getId().equals(company.getId())) {
                    mCompanies.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
