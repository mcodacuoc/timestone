package uoc.edu.timestone.manager;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.User;

/**
 * This class handles the creation, update, removes and query for users.
 */
public class UserManager implements ChildEventListener {
    private DatabaseReference mRef;
    private List<User> mUsers = new ArrayList<>();
    static final String kDatabasereference = "users";

    /**
     * UserManager class constructor
     *
     * @param database The firebase database reference
     */
    public UserManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public UserManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    /**
     * This method adds new user to database
     *
     * @param dni      The user dni
     * @param email    The user email
     * @param password The user password
     * @param name     The user name
     * @param surname1 The user first surname
     * @param surname2 The user second surname
     * @param position The user position in company
     * @param dayhours The number of working hours per day stipulated by contract
     */
    public void addUser(String dni, String email, String password, String name,
                        String surname1, String surname2, String position, int dayhours) {
        User user = new User(dni, email, password, name, surname1, surname2, position, dayhours);
        mRef.child(dni).setValue(user);
    }

    /**
     * This method updates user information in database
     *
     * @param user The user to be updated
     */
    public void updateUser(User user) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(user.getId(), user);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes an user from database
     *
     * @param user The user to be removed
     */
    public void removeUser(User user) {
        mRef.child(user.getId()).removeValue();
    }

    /**
     * This method returns the list of users
     *
     * @return The current list of users
     */
    public List<User> getUsers() {
        return mUsers;
    }

    /**
     * This method returns an specified user by dni
     *
     * @param dni The user dni to be found
     * @return The specified user. Returns null if no exists.
     */
    public User getUser(String dni) {
        User result = null;

        for (User user : mUsers) {
            if (user.getId().equals(dni)) {
                result = user;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns an specified user by email
     *
     * @param email The user email to be found
     * @return The specified user. Returns null if no exists
     */
    public User getUserByEmail(String email) {
        User result = null;

        for (User user : mUsers) {
            if (user.getEmail().equals(email)) {
                result = user;
                break;
            }
        }

        return result;
    }


    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mUsers.add(dataSnapshot.getValue(User.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        User user = dataSnapshot.getValue(User.class);
        if (null != user) {
            for (int i = 0; i < mUsers.size(); i++) {
                if (mUsers.get(i).getId().equals(user.getId())) {
                    mUsers.set(i, user);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        User user = dataSnapshot.getValue(User.class);
        if (null != user) {
            for (int i = 0; i < mUsers.size(); i++) {
                if (mUsers.get(i).getId().equals(user.getId())) {
                    mUsers.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
