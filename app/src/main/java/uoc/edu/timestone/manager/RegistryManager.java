package uoc.edu.timestone.manager;

import android.os.Build;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.utils.DateTimeUtils;

/**
 * This class handles the creation, update, removes and query for registries.
 */
public class RegistryManager implements ChildEventListener {
    private DatabaseReference mRef;
    private List<Registry> mRegistries = new ArrayList<>();
    static final String kDatabasereference = "registry";

    /**
     * RegistryManager class constructor
     *
     * @param database The firebase database reference
     */
    public RegistryManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public RegistryManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    /**
     * This method adds a new registry to database
     *
     * @param center    The center in which the registry occurs
     * @param event     The event to be registered
     * @param user      The user to be registered
     * @param date      The date of the registry
     * @param time      The time of the registry
     * @param confirmed <code>true</code> if the registry have been confirmed by user
     */
    public void addRegistry(Center center, Event event, User user, String date,
                            String time, boolean confirmed) {
        String key = mRef.push().getKey();
        if (null != key) {
            Registry registry = new Registry(key, center, event, user, date, time, confirmed);
            mRef.child(key).setValue(registry);
        }
    }

    /**
     * This method updates a registry from database
     *
     * @param registry The registry to be removed
     */
    public void updateRegistry(Registry registry) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(registry.getId(), registry);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes a registry from database
     *
     * @param registry The registry to be removed
     */
    public void removeRegistry(Registry registry) {
        mRef.child(registry.getId()).removeValue();
    }

    /**
     * This method returns the list of registries
     *
     * @return The current list of registries
     */
    public List<Registry> getRegistries() {
        return mRegistries;
    }

    /**
     * This method returns a registry specified by id
     *
     * @param id The identifier to be found
     * @return The registry found. Returns null if no exists.
     */
    public Registry getRegistry(String id) {
        Registry result = null;

        for (Registry registry : mRegistries) {
            if (registry.getId().equals(id)) {
                result = registry;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns a registry list filtered by user and date
     *
     * @param userId The user identifier to filter by
     * @param date   The date to filter by
     * @return The list of registries filtered by user and date
     */
    public List<Registry> getRegistriesFor(final String userId, final String date) {
        List<Registry> resultList = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resultList = mRegistries.stream()
                    .filter(new Predicate<Registry>() {
                        @Override
                        public boolean test(Registry r) {
                            return r.getUserId().equals(userId);
                        }
                    })
                    .filter(new Predicate<Registry>() {
                        @Override
                        public boolean test(Registry r) {
                            return r.getDate().equals(date);
                        }
                    })
                    .sorted(new Comparator<Registry>() {
                        @Override
                        public int compare(Registry o1, Registry o2) {
                            int result = 1;
                            Date o1Timestamp;
                            Date o2Timestamp;
                            try {
                                o1Timestamp = DateTimeUtils.toTimestamp(o1.getDate(), o1.getTime());
                                o2Timestamp = DateTimeUtils.toTimestamp(o2.getDate(), o2.getTime());
                            } catch (ParseException e) {
                                return result;
                            }

                            if (o1Timestamp.after(o2Timestamp)) {
                                result = 1;
                            } else if (o1Timestamp.before(o2Timestamp)) {
                                result = -1;
                            } else {
                                result = 0;
                            }
                            return result;
                        }
                    })
                    .collect(Collectors.<Registry>toList());
        }

        return resultList;
    }

    /**
     * This method returns a registry list filtered by center, user and date ranges
     *
     * @param centerId        The center identifier to be filtered by
     * @param userId          The user identifier to be filtered by
     * @param fromDateString  The initial range date
     * @param untilDateString The final range date
     * @return The filtered list of registries
     */
    public List<Registry> getRegistriesFor(final String centerId, final String userId,
                                           final String fromDateString,
                                           final String untilDateString) {
        List<Registry> resultList = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resultList = mRegistries.stream()
                    .filter(new Predicate<Registry>() {
                        @Override
                        public boolean test(Registry r) {
                            return r.getUserId().equals(userId);
                        }
                    }).filter(new Predicate<Registry>() {
                        @Override
                        public boolean test(Registry r) {
                            return r.getCenterId().equals(centerId);
                        }
                    })
                    .filter(new Predicate<Registry>() {
                        @Override
                        public boolean test(Registry r) {
                            boolean result = false;
                            Date fromDate;
                            Date untilDate;
                            Date rTimestamp;

                            try {
                                fromDate = DateTimeUtils.toTimestamp(fromDateString, "00:00:00");
                                untilDate = DateTimeUtils.toTimestamp(untilDateString, "23:59:59");
                                rTimestamp = DateTimeUtils.toTimestamp(r.getDate(), r.getTime());
                            } catch (ParseException e) {
                                return result;
                            }

                            return (rTimestamp.after(fromDate) || rTimestamp.equals(fromDate)) &&
                                    (rTimestamp.before(untilDate) || rTimestamp.equals(untilDate));
                        }
                    })
                    .sorted(new Comparator<Registry>() {
                        @Override
                        public int compare(Registry o1, Registry o2) {
                            int result = 1;
                            Date o1Timestamp;
                            Date o2Timestamp;
                            try {
                                o1Timestamp = DateTimeUtils.toTimestamp(o1.getDate(), o1.getTime());
                                o2Timestamp = DateTimeUtils.toTimestamp(o2.getDate(), o2.getTime());
                            } catch (ParseException e) {
                                return result;
                            }

                            if (o1Timestamp.after(o2Timestamp)) {
                                result = 1;
                            } else if (o1Timestamp.before(o2Timestamp)) {
                                result = -1;
                            } else {
                                result = 0;
                            }
                            return result;
                        }
                    })
                    .collect(Collectors.<Registry>toList());
        }

        return resultList;
    }


    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mRegistries.add(dataSnapshot.getValue(Registry.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        Registry registry = dataSnapshot.getValue(Registry.class);
        if (null != registry) {
            for (int i = 0; i < mRegistries.size(); i++) {
                if (mRegistries.get(i).getId().equals(registry.getId())) {
                    mRegistries.set(i, registry);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Registry registry = dataSnapshot.getValue(Registry.class);
        if (null != registry) {
            for (int i = 0; i < mRegistries.size(); i++) {
                if (mRegistries.get(i).getId().equals(registry.getId())) {
                    mRegistries.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
