package uoc.edu.timestone.manager;

import android.os.Build;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.utils.DateTimeUtils;

/**
 * This class handles the creation, update, removes and query for devices.
 */
public class DeviceManager implements ChildEventListener {
    private DatabaseReference mRef;
    private List<Device> mDevices = new ArrayList<>();
    static final String kDatabasereference = "devices";

    private static final Map<Device.DeviceType, String> deviceTypeMap;

    static {
        deviceTypeMap = new HashMap<>();
        deviceTypeMap.put(Device.DeviceType.DEVICE_TYPE_ENTRANCE, "Entrada");
        deviceTypeMap.put(Device.DeviceType.DEVICE_TYPE_INTERIOR, "Interior");
    }

    /**
     * DeviceManager class constructor
     *
     * @param database The firebase database reference
     */
    public DeviceManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public DeviceManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    public String getDeviceTypeString(Device.DeviceType type) {
        return deviceTypeMap.get(type);
    }

    /**
     * This method adds a new device to database
     *
     * @param uuid   The device identifier
     * @param major  The device major number
     * @param minor  The device minor number
     * @param type   The type of device
     * @param center The center which device belongs to
     */
    public void addDevice(String uuid, int major, int minor, Device.DeviceType type, Center center) {
        Device device = new Device(uuid, major, minor, type, center);
        mRef.child(uuid).setValue(device);
    }

    /**
     * This method updates the specified device in database
     *
     * @param device The device to be updated
     */
    public void updateDevice(Device device) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(device.getId(), device);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes the specified device from database
     *
     * @param device The device to be removed
     */
    public void removeDevice(Device device) {
        mRef.child(device.getId()).removeValue();
    }

    /**
     * This method returns the list of devices
     *
     * @return The current list of devices
     */
    public List<Device> getDevices() {
        return mDevices;
    }

    /**
     * This method returns the specified device
     *
     * @param uuid The identifier of device to be found
     * @return The specified device. Returns null if no exists.
     */
    public Device getDevice(String uuid) {
        Device result = null;

        for (Device device : mDevices) {
            if (device.getId().equals(uuid)) {
                result = device;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns a list of devices related to specified center and type
     *
     * @param centerId The center identifier to filter by
     * @param type     The device type to filter by
     * @return A list of devices of the specified type and for the given center
     */
    public List<Device> getDevicesFor(final String centerId, final Device.DeviceType type) {
        List<Device> resultList = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resultList = mDevices.stream()
                    .filter(new Predicate<Device>() {
                        @Override
                        public boolean test(Device d) {
                            return d.getCenterId().equals(centerId);
                        }
                    })
                    .filter(new Predicate<Device>() {
                        @Override
                        public boolean test(Device d) {
                            return d.getType().equals(type);
                        }
                    })
                    .collect(Collectors.<Device>toList());
        }

        return resultList;
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mDevices.add(dataSnapshot.getValue(Device.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        Device device = dataSnapshot.getValue(Device.class);
        if (null != device) {
            for (int i = 0; i < mDevices.size(); i++) {
                if (mDevices.get(i).getId().equals(device.getId())) {
                    mDevices.set(i, device);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Device device = dataSnapshot.getValue(Device.class);
        if (null != device) {
            for (int i = 0; i < mDevices.size(); i++) {
                if (mDevices.get(i).getId().equals(device.getId())) {
                    mDevices.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
