package uoc.edu.timestone.manager;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Company;

/**
 * This class handles the creation, update, removes and query for centers.
 */
public class CenterManager implements ChildEventListener {
    private DatabaseReference mRef;
    private List<Center> mCenters = new ArrayList<>();
    static final String kDatabasereference = "centers";

    /**
     * CenterManager class constructor
     *
     * @param database The firebase database reference
     */
    public CenterManager(FirebaseDatabase database) {
        mRef = database.getReference(kDatabasereference);
        mRef.addChildEventListener(this);
    }

    public CenterManager(FirebaseDatabase database, String baseRef) {
        mRef = database.getReference(baseRef + "/" + kDatabasereference);
        mRef.addChildEventListener(this);
    }

    /**
     * This method adds a ValueEventListener for handle database changes
     *
     * @param listener ValueEventListener associated
     */
    public void addValueEventListener(ValueEventListener listener) {
        mRef.addValueEventListener(listener);
    }

    /**
     * This method adds a new center to database
     *
     * @param name    The name of the center
     * @param city    The city in which the center is located
     * @param address The address of the center
     * @param cp      The postal code
     * @param company The company to which the center belongs
     */
    public void addCenter(String name, String city, String address, int cp, Company company) {
        String key = mRef.push().getKey();
        if (null != key) {
            Center center = new Center(key, name, city, address, cp, company);
            mRef.child(key).setValue(center);
        }
    }

    /**
     * This method updates the specified center in database
     *
     * @param center Center to be updated
     */
    public void updateCenter(Center center) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(center.getId(), center);
        mRef.updateChildren(childUpdates);
    }

    /**
     * This method removes the specified center from database
     *
     * @param center The center to be removed
     */
    public void removeCenter(Center center) {
        mRef.child(center.getId()).removeValue();
    }

    /**
     * This method returns the list of centers
     *
     * @return The current list of centers
     */
    public List<Center> getCenters() {
        return mCenters;
    }

    /**
     * This method returns the center specified by id
     *
     * @param id The center identifier
     * @return The center specified by id. Returns null if no exists.
     */
    public Center getCenter(String id) {
        Center result = null;

        for (Center center : mCenters) {
            if (center.getId().equals(id)) {
                result = center;
                break;
            }
        }

        return result;
    }

    /**
     * This method returns the center specified by name
     *
     * @param name The center name
     * @return The center specified by name. Returns null if no exists.
     */
    public Center getCenterByName(String name) {
        Center result = null;

        for (Center center : mCenters) {
            if (center.getName().equals(name)) {
                result = center;
                break;
            }
        }

        return result;
    }


    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        mCenters.add(dataSnapshot.getValue(Center.class));
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        Center center = dataSnapshot.getValue(Center.class);
        if (null != center) {
            for (int i = 0; i < mCenters.size(); i++) {
                if (mCenters.get(i).getId().equals(center.getId())) {
                    mCenters.set(i, center);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        Center center = dataSnapshot.getValue(Center.class);
        if (null != center) {
            for (int i = 0; i < mCenters.size(); i++) {
                if (mCenters.get(i).getId().equals(center.getId())) {
                    mCenters.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("UserManager", "loadUser:onCancelled", databaseError.toException());
    }
}
