package uoc.edu.timestone;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.User;
import uoc.edu.timestone.ui.login.LoginActivity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * This class represents an empty activity used to choose between login or
 * main application activity
 */
public class MainEmptyActivity extends AppCompatActivity implements ValueEventListener {
    private static final int PERMISSION_REQUEST = 1;
    static final String TAG = "MainEmptyActivity";
    static public User mCurrentUser;
    private UserManager mUserManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initializes user manager
        mUserManager = new UserManager(FirebaseDatabase.getInstance());
        mUserManager.addValueEventListener(this);

        // Requests user for location permissions
        this.requestPermission();
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        int mode = Activity.MODE_PRIVATE;
        SharedPreferences mySharedPreferences;
        mySharedPreferences = getSharedPreferences("timestone_login", mode);

        //Retrieve the saved values.
        String email = mySharedPreferences.getString("email", "");
        String password = mySharedPreferences.getString("password", "");

        Intent activityIntent;
        User user = mUserManager.getUserByEmail(email);

        // Go straight to main if user have been previously logged, go to login
        // screen otherwise
        if (null != user && null != email && user.getEmail().equals(email) &&
                null != password && user.getPassword().equals(password)) {
            activityIntent = new Intent(this, MainActivity.class);
        } else {
            activityIntent = new Intent(this, LoginActivity.class);
        }

        mCurrentUser = user;
        startActivity(activityIntent);
        finish();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.e(TAG, "Loading registered users failed");
        finish();
    }

    /**
     * This method requests users to grant location permissions to application
     */
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    this.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (0 < grantResults.length) {
            switch (requestCode) {
                case PERMISSION_REQUEST: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("MainEmptyActivity", "location permission granted");

                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle(getString(R.string.permission_dialog_title));
                        builder.setMessage(getString(R.string.permission_dialog_content));
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }
                        });
                        builder.show();
                    }
                }
            }
        }
    }
}
