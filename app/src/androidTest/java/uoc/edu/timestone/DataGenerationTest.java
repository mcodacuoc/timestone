package uoc.edu.timestone;

import com.google.firebase.database.FirebaseDatabase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.CompanyManager;
import uoc.edu.timestone.manager.DeviceManager;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.utils.DateTimeUtils;

import static android.os.SystemClock.sleep;

/**
 * This test generates a set of test data
 */
public class DataGenerationTest {
    private UserManager mUserManager;
    private CompanyManager mCompanyManager;
    private CenterManager mCenterManager;
    private EventManager mEventManager;
    private DeviceManager mDeviceManager;
    private RegistryManager mRegistryManager;
    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();

    @Before
    public void createManagers() {
        mDatabase.getReference().removeValue();
        mCenterManager = new CenterManager(mDatabase);
        mCompanyManager = new CompanyManager(mDatabase);
        mDeviceManager = new DeviceManager(mDatabase);
        mUserManager = new UserManager(mDatabase);
        mEventManager = new EventManager(mDatabase);
        mRegistryManager = new RegistryManager(mDatabase);

        generateData();
        sleep(5000);
    }

    @Test
    public void emptyTest() {
        Assert.assertTrue(true);
    }

    /**
     * This method generates a set of data to be used in in testing procedures
     */
    public void generateData() {
        mUserManager.addUser("333333E", "admin@company.es", "admin",
                "Admin", "", "", "admin", 8);
        mUserManager.addUser("444444E", "jane_doe@company.es", "password",
                "Jane", "Doe", "", "RRHH", 8);
        mUserManager.addUser("555555E", "john_doe@company.es", "password",
                "John", "Doe", "", "Worker", 8);

        if (null == mEventManager.getEvent(EventManager.ENTER_EVENT_ID)) {
            Event event = new Event("0", "Entrada", true,
                    Event.EventType.EVENT_TYPE_ENTER, "#00FF00");
            mDatabase.getReference("events").child("0").setValue(event);
        }

        if (null == mEventManager.getEvent(EventManager.END_EVENT_ID)) {
            Event event = new Event("1", "Fin de jornada", true,
                    Event.EventType.EVENT_TYPE_EXIT, "#FF0000");
            mDatabase.getReference("events").child("1").setValue(event);
        }

        if (null == mEventManager.getEventByName("Descanso")) {
            mEventManager.addEvent("Descanso", true, Event.EventType.EVENT_TYPE_EXIT,
                    "#AF6789");
        }

        if (null == mEventManager.getEventByName("Comida")) {
            mEventManager.addEvent("Comida", false, Event.EventType.EVENT_TYPE_EXIT,
                    "#6789AF");
        }

        if (null == mEventManager.getEventByName("Salida laboral")) {
            mEventManager.addEvent("Salida laboral", true, Event.EventType.EVENT_TYPE_EXIT, "#89AF67");
        }
        sleep(3000);

        mCompanyManager.addCompany("1234567W", "MegaCorp");
        sleep(3000);

        mCenterManager.addCenter("Central", "Capital City", "Elm Street S/N",
                00001, mCompanyManager.getCompany("1234567W"));
        sleep(3000);

        mDeviceManager.addDevice("00000000-0000-0000-C000-000000000046", 1234, 5678,
                Device.DeviceType.DEVICE_TYPE_ENTRANCE, mCenterManager.getCenterByName("Central"));
        sleep(3000);

        int year = 2020;
        for (int i = 0; i < 12; i++) {
            Calendar myCal = Calendar.getInstance();
            myCal.set(year, i, 1, 0, 0, 0);
            int daysOfMonth = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);

            for (int j = 0; j < daysOfMonth; j++) {

                myCal.set(year, i, j + 1, 0, 0, 0);
                String dateString = DateTimeUtils.toDateString(myCal.getTime());

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.ENTER_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "09:00:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEventByName("Descanso"),
                        mUserManager.getUser("555555E"), dateString, "10:00:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.ENTER_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "10:30:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEventByName("Comida"),
                        mUserManager.getUser("555555E"), dateString, "14:30:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.ENTER_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "15:00:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.END_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "17:30:00",
                        true);
            }
        }
    }
}
