package uoc.edu.timestone;

import com.google.firebase.database.FirebaseDatabase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.CompanyManager;
import uoc.edu.timestone.manager.DeviceManager;
import uoc.edu.timestone.manager.EventManager;
import uoc.edu.timestone.manager.RegistryManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Device;
import uoc.edu.timestone.model.Event;
import uoc.edu.timestone.model.Registry;
import uoc.edu.timestone.utils.DateTimeUtils;
import uoc.edu.timestone.utils.EntryData;
import uoc.edu.timestone.utils.WorkingHourUtils;

import static android.os.SystemClock.sleep;

/**
 * This class implements several tests to check if working hours calculations are well defined.
 */
public class WorkingHoursTest {
    private UserManager mUserManager;
    private CompanyManager mCompanyManager;
    private CenterManager mCenterManager;
    private EventManager mEventManager;
    private DeviceManager mDeviceManager;
    private RegistryManager mRegistryManager;
    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();

    @Before
    public void createManagers() {
        mDatabase.getReference().removeValue();
        mCenterManager = new CenterManager(mDatabase);
        mCompanyManager = new CompanyManager(mDatabase);
        mDeviceManager = new DeviceManager(mDatabase);
        mUserManager = new UserManager(mDatabase);
        mEventManager = new EventManager(mDatabase);
        mRegistryManager = new RegistryManager(mDatabase);

        generateData();
        sleep(5000);
    }

    @Test
    public void calculateMonthHours() {
        WorkingHourUtils workingHourUtils = new WorkingHourUtils(mRegistryManager, mEventManager);
        long total = workingHourUtils.calculateWorkTime("2020",
                "Enero", "555555E");
        long hours = (((total / 1000) / 60) / 60);
        Assert.assertEquals((8 * 31), hours);
    }

    @Test
    public void calculatedChartEntries() {
        WorkingHourUtils workingHourUtils = new WorkingHourUtils(mRegistryManager, mEventManager);
        EntryData entryData = new EntryData();
        List<Registry> registryList = mRegistryManager.
                getRegistriesFor(mCenterManager.getCenterByName("Central").getId(), "555555E",
                        "01/01/2020", "31/12/2020");

        workingHourUtils.updateChartEntryData(registryList, entryData);

        long total = entryData.getTotalTime();
        float hours = (((total / 1000) / 60) / 60);

        Assert.assertEquals((8.5 * 366), hours, 0.01);

        Iterator<String> itr = entryData.getEventMap().keySet().iterator();
        while (itr.hasNext()) {

            // Obtains number of hours value and event
            String key = itr.next();
            Long value = entryData.getEventMap().get(key).first;

            if (key.equals(EventManager.ENTER_EVENT_ID)) {
                Assert.assertEquals((88.2353), (float) value * 100 / total, 0.01);
            } else {
                Assert.assertEquals((5.882353), (float) value * 100 / total, 0.01);
            }
        }
    }

    public void generateData() {
        mUserManager.addUser("444444E", "jane_doe@company.es", "password",
                "Jane", "Doe", "", "RRHH", 8);
        mUserManager.addUser("555555E", "john_doe@company.es", "password",
                "John", "Doe", "", "Worker", 8);

        if (null == mEventManager.getEvent(EventManager.ENTER_EVENT_ID)) {
            Event event = new Event("0", "Entrada", true,
                    Event.EventType.EVENT_TYPE_ENTER, "#00FF00");
            mDatabase.getReference("events").child("0").setValue(event);
        }

        if (null == mEventManager.getEvent(EventManager.END_EVENT_ID)) {
            Event event = new Event("1", "Fin de jornada", true,
                    Event.EventType.EVENT_TYPE_EXIT, "#FF0000");
            mDatabase.getReference("events").child("1").setValue(event);
        }

        if (null == mEventManager.getEventByName("Descanso")) {
            mEventManager.addEvent("Descanso", true, Event.EventType.EVENT_TYPE_EXIT,
                    "#AF6789");
        }

        if (null == mEventManager.getEventByName("Comida")) {
            mEventManager.addEvent("Comida", false, Event.EventType.EVENT_TYPE_EXIT,
                    "#6789AF");
        }

        if (null == mEventManager.getEventByName("Salida laboral")) {
            mEventManager.addEvent("Salida laboral", true, Event.EventType.EVENT_TYPE_EXIT, "#89AF67");
        }
        sleep(3000);

        mCompanyManager.addCompany("1234567W", "MegaCorp");
        sleep(3000);

        mCenterManager.addCenter("Central", "Capital City", "Elm Street S/N",
                00001, mCompanyManager.getCompany("1234567W"));
        sleep(3000);

        mDeviceManager.addDevice("00000000-0000-0000-C000-000000000046", 1234, 5678,
                Device.DeviceType.DEVICE_TYPE_ENTRANCE, mCenterManager.getCenterByName("Central"));
        sleep(3000);

        int year = 2020;
        for (int i = 0; i < 12; i++) {
            Calendar myCal = Calendar.getInstance();
            myCal.set(year, i, 1, 0, 0, 0);
            int daysOfMonth = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);

            for (int j = 0; j < daysOfMonth; j++) {

                myCal.set(year, i, j + 1, 0, 0, 0);
                String dateString = DateTimeUtils.toDateString(myCal.getTime());

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.ENTER_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "09:00:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEventByName("Descanso"),
                        mUserManager.getUser("555555E"), dateString, "10:00:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.ENTER_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "10:30:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEventByName("Comida"),
                        mUserManager.getUser("555555E"), dateString, "14:30:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.ENTER_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "15:00:00",
                        true);

                mRegistryManager.addRegistry(mCenterManager.getCenterByName("Central"),
                        mEventManager.getEvent(EventManager.END_EVENT_ID),
                        mUserManager.getUser("555555E"), dateString, "17:30:00",
                        true);
            }
        }
    }
}
