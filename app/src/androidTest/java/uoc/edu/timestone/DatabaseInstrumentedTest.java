package uoc.edu.timestone;

import com.google.firebase.database.FirebaseDatabase;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uoc.edu.timestone.manager.CenterManager;
import uoc.edu.timestone.manager.CompanyManager;
import uoc.edu.timestone.manager.UserManager;
import uoc.edu.timestone.model.Center;
import uoc.edu.timestone.model.Company;
import uoc.edu.timestone.model.User;

import static android.os.SystemClock.sleep;

public class DatabaseInstrumentedTest {
    private UserManager mUserManager;
    private CompanyManager mCompanyManager;
    private CenterManager mCenterManager;
    FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();

    @Before
    public void createManagers() {
        mDatabase.getReference("test").removeValue();
        mUserManager = new UserManager(mDatabase, "test");
        mCompanyManager = new CompanyManager(mDatabase, "test");
        mCenterManager = new CenterManager(mDatabase, "test");
    }

    @After
    public void cleanDatabase() {
        mDatabase.getReference("test").removeValue();
    }

    @Test
    public void test1() {
        mUserManager.addUser("75766461E", "moises.coda@gmail.com", "password", "Moisés",
                "Coda", "Cabeza de Vaca", "Analista", 8);
        sleep(2000);

        User user1 = mUserManager.getUser("75766461E");
        if (user1 != null) {
            Assert.assertEquals(user1.getId(), "75766461E");
        } else {
            Assert.fail("User was null");
        }
    }

    @Test
    public void test2() {
        mUserManager.addUser("75766462E", "rodrigo.coda@gmail.com", "password", "Rodrigo",
                "Coda", "Cabeza de Vaca", "Analista", 8);
        sleep(2000);

        User user1 = mUserManager.getUser("75766462E");
        if (user1 != null) {
            Assert.assertEquals(user1.getName(), "Rodrigo");
            user1.setName("Alonso");
            mUserManager.updateUser(user1);
            sleep(2000);

            User user2 = mUserManager.getUser("75766462E");
            Assert.assertEquals(user2.getName(), "Alonso");
        } else {
            Assert.fail("User was null");
        }
    }


    @Test
    public void test3() {
        mUserManager.addUser("75766463E", "rodrigo.coda@gmail.com", "password", "Rodrigo",
                "Coda", "Cabeza de Vaca", "Analista", 8);
        sleep(2000);

        User user1 = mUserManager.getUser("75766463E");
        mUserManager.removeUser(user1);
        sleep(2000);

        User user2 = mUserManager.getUser("75766463E");
        Assert.assertNull(user2);
    }

    @Test
    public void test4() {
        mCompanyManager.addCompany("E12345678", "SOOLOGIC");
        sleep(2000);

        Company company = mCompanyManager.getCompany("E12345678");
        mCenterManager.addCenter("Sevilla - Centro", "Sevilla", "C/ Trille Nº 29", 11012, company);
        sleep(2000);

        Center center = mCenterManager.getCenterByName("Sevilla - Centro");
        Company companyAux = mCompanyManager.getCompany(center.getCompanyId());
        Assert.assertEquals(center.getName(), "Sevilla - Centro");
        Assert.assertEquals(companyAux.getName(), "SOOLOGIC");
    }
}